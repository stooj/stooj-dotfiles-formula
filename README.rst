==============
stooj_dotfiles
==============

Install dotfiles for stooj

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.


Available states
================

.. contents::
    :local:

``stooj_dotfiles``
------------------

Install dotfiles for stooj


Configured applications
=======================

abcde
-----

ABCDE will only be installed and configured if the target machine has a grain
called `has_optical` which is set to True.

Git
---



Template
========

This formula was created from a cookiecutter template.

See https://github.com/mitodl/saltstack-formula-cookiecutter.


Font characters
===============

I've found it really difficult to work out what characters exist in a patched
font. `This tool by bluejamesbond
<http://bluejamesbond.github.io/CharacterMap/>`_ solves that.
