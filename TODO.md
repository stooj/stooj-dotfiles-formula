# Configs to add

## VirtualBox

- `$HOME/.config/VirtualBox/VirtualBox.xml`

## Aur packages

- rofi-pass
- clerk
- mopidy-iris (no debian package)
- mopidy-local-images (no debian package) (no aur package, use mopidy-local)
- mopidy-mpd
- mopidy-moped (no debian package)
- mopidy-mopify (no debian package)
- mopidy-podcast
- mopidy-simple-webclient (no debian package)
- mopidy-soundcloud
- mopidy-spotify (no debian package)
- todotxt
- libolm (for weechat matrix)

## Not available on debian

- teiler

## Other

- livestreamer or streamlink
- liferea?
- irssi?
- cmus

# Configs to update

## Vim
Vim config is quite out of date. It should be brought into line with my neovim
config for a similar experience.

## Notmuch

Cio laptop has a working notmuch config. It needs to be tweaked to exclude
mairix directory though

# Other considerations

## Screenshot tools

- I've heard nice things about [maim](https://github.com/naelstrof/maim)
- Also [flameshot](https://github.com/lupoDharkael/flameshot), but maybe a bit
    gooey? It has a built-in blur tool though, which is really grand.

  ```bash
  bindsym Print       exec flameshot full
  bindsym Shift+Print exec flameshot gui
  ```

- Also [ksnip](https://github.com/DamirPorobic/ksnip)

## Redo the bars

Check [Your Guide to a Practical Linux Desktop With i3WM](https://www.devpy.me/your-guide-to-a-practical-linux-desktop-with-i3wm/) for some bar ideas. Do I really want to change my current bar?
