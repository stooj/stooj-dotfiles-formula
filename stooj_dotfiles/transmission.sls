{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_transmission_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.transmission|yaml }}

stooj_dotfiles_transmission_config_managed:
  file.managed:
    - name: {{ home }}/.config/transmission/settings.json
    - source: salt://stooj_dotfiles/files/transmission/settings.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
