{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_manage_dosbox:
  file.managed:
    - name: {{ home }}/.dosbox/dosbox-0.74.conf
    - source: salt://stooj_dotfiles/files/dosbox/dosbox-0.74.conf
    - user: {{ username }}
    - group: {{ username }}
    - filemode: 0644
    - dirmode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    - require:
      - user: {{ username }}
