{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = stooj_dotfiles.username %}
stooj_dotfiles_ensure_user_exists:
  user.present:
    - name: {{ username }}
