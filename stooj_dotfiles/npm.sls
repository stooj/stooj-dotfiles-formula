{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_npm_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.npm|yaml }}

stooj_dotfiles_npm_rc_managed:
  file.managed:
    - name: {{ home }}/.npmrc
    - source: salt://stooj_dotfiles/files/npm/npmrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

{% for package in stooj_dotfiles.npm.pkgs %}
stooj_dotfiles_node_{{ package }}_installed:
  npm.installed:
    - name: {{ package }}
    - user: {{ username }}
    - require:
      - stooj_dotfiles_npm_installed
      - user: {{ username }}
{% endfor %}
