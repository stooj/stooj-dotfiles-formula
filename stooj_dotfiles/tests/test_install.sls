{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% for pkg in stooj_dotfiles.pkgs %}
test_{{pkg}}_is_installed:
  testinfra.package:
    - name: {{ pkg }}
    - is_installed: True
{% endfor %}
