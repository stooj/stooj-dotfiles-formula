{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_urxvt_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.urxvt|yaml }}

{% for location in [
  'indoors',
  'outdoors',
  ]
%}
stooj_dotfiles_urxvt_{{ location }}_config_managed:
  file.managed:
    - name: {{ home }}/.Xresources.d/urxvt.{{ location }}
    - source: salt://stooj_dotfiles/files/urxvt/urxvt.{{ location }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for script in [
  'term-recolor',
  'toggle-outdoors',
  ]
%}
stooj_dotfiles_urxvt_{{ script|replace('-', '_') }}_script_managed:
  file.managed:
    - name: {{ home }}/bin/{{ script }}
    - source: salt://stooj_dotfiles/files/urxvt/{{ script }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}
