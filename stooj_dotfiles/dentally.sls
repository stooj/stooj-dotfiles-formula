{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set osfamily = salt['grains.get']('os_family', None) %}
{% set os = salt['grains.get']('os', None) %}
{% set home = "/home/%s" % username %}


stooj_dotfiles_dentally_directory_managed:
  file.directory:
    - name: {{ home }}/programming/dentally
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

stooj_dotfiles_dentally_dentally_repo_cloned:
  git.latest:
    - name: git@gitlab.com:dentally/dentally.git
    - target: {{ home }}/programming/dentally/dentally
    - user: {{ username }}
    - branch: develop
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_dentally_directory_managed

{# Dentally requires postgres 9.6
# stooj_dotfiles_dentally_postgresql_installed:
#   pkg.installed:
#     - pkgs: {{ stooj_dotfiles.dentally.requirements.postgresql|yaml }}
#}

{#
# {% if osfamily == 'Arch' %}
# stooj_dotfiles_dentally_postgresql_db_initialized:
#   cmd.run:
#     - name: initdb --locale en_GB.UTF-8 -E UTF8 -D '/var/lib/postgres/data'
#     - runas: postgres
#     - require:
#       - stooj_dotfiles_dentally_postgresql_installed
#     - creates: /var/lib/postgres/data/postgresql.conf
# {% endif %}
#}

{#
# stooj_dotfiles_dentally_postgresql_service_enabled:
#   service.running:
#     - name: postgresql
#     - enable: True
#     - require:
#       - stooj_dotfiles_dentally_postgresql_installed
#       {% if osfamily == 'Arch' %}
#       - stooj_dotfiles_dentally_postgresql_db_initialized
#       {% endif %}
#}

stooj_dotfiles_dentally_bundler_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.dentally.requirements.bundler|yaml }}

stooj_dotfiles_dentally_redis_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.dentally.requirements.redis|yaml }}

stooj_dotfiles_dentally_hosts_configured:
  host.present:
    - ip: 127.0.0.1
    - names:
      - app.rails.localhost
      - api.rails.localhost

stooj_dotfiles_dentally_aws_config_directory_exists:
  file.directory:
    - name: {{ home }}/.aws
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 700

stooj_dotfiles_dentally_aws_config_file_managed:
  file.managed:
    - name: {{ home }}/.aws/config
    - source: salt://stooj_dotfiles/templates/aws/config.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.dentally.aws|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_dentally_aws_config_directory_exists

stooj_dotfiles_dentally_aws_credentials_file_managed:
  file.managed:
    - name: {{ home }}/.aws/credentials
    - source: salt://stooj_dotfiles/templates/aws/credentials.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.dentally.aws|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_dentally_aws_config_directory_exists

stooj_dotfiles_dentally_aws_s3fs_file_managed:
  file.managed:
    - name: {{ home }}/.passwd-s3fs
    - source: salt://stooj_dotfiles/templates/aws/passwd-s3fs.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.dentally.aws|json }}
    - require:
      - user: {{ username }}

stooj_dotfiles_dentally_env_development_file_managed:
  file.managed:
    - name: {{ home }}/programming/dentally/dentally/.env.development
    - source: salt://stooj_dotfiles/templates/dentally/dev.environment.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.dentally.envvars|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_dentally_dentally_repo_cloned

stooj_dotfiles_dentally_env_test_file_managed:
  file.managed:
    - name: {{ home }}/programming/dentally/dentally/.env.test
    - source: salt://stooj_dotfiles/templates/dentally/dev.environment.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.dentally.test_envvars|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_dentally_dentally_repo_cloned

stooj_dotfiles_dentally_ruby_version_file_created:
  file.managed:
    - name: {{ home }}/programming/dentally/dentally/.ruby-version
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - contents: {{ stooj_dotfiles.dentally.envvars.RBENV_VERSION }}
    - require:
      - stooj_dotfiles_dentally_dentally_repo_cloned

stooj_dotfiles_dentally_spec_lint_directory_created:
  file.directory:
    - name: {{ home }}/programming/dentally/dentally/spec/lint
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - require:
      - stooj_dotfiles_dentally_dentally_repo_cloned

stooj_dotfiles_dentally_spec_helper_file_managed:
  file.managed:
    - name: {{ home }}/programming/dentally/dentally/spec/lint/rubocop_spec.rb
    - source: salt://stooj_dotfiles/files/dentally/rubocop_spec.rb
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - require:
      - stooj_dotfiles_dentally_dentally_repo_cloned
      - stooj_dotfiles_dentally_spec_lint_directory_created
{#
# stooj_dotfiles_dentally_postgres_user_created:
#   postgres_user.present:
#     - name: {{ stooj_dotfiles.dentally.envvars.PGUSER }}
#     - superuser: True
#     - password: {{ stooj_dotfiles.dentally.envvars.PGPASSWORD }}
#     - require:
#       - stooj_dotfiles_dentally_postgresql_installed
#}

{#
# stooj_dotfiles_dentally_postgres_database_created:
#   postgres_database.present:
#     - name: {{ stooj_dotfiles.dentally.envvars.PGDATABASE }}
#     - owner: {{ stooj_dotfiles.dentally.envvars.PGUSER }}
#     - require:
#       - stooj_dotfiles_dentally_postgres_user_created
#}

stooj_dotfiles_dentally_wiki_cloned:
  git.latest:
    - name: git@gitlab.com:dentally/dentally.wiki.git
    - target: {{ home }}/programming/dentally/dentally.wiki
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_dentally_directory_managed

stooj_dotfiles_dentally_docs_virtualenv_script_downloaded:
  file.managed:
    - name: /tmp/create_dentally_virtualenvs
    - source: salt://stooj_dotfiles/templates/dentally/create_virtualenvs.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
    - unless:
      - ls {{ home }}/.virtualenvs/dentally-docs

stooj_dotfiles_dentally_docs_virtualenv_created:
  cmd.run:
    - name: bash /tmp/create_dentally_virtualenvs
    - runas: {{ username }}
    - cwd: {{ home }}
    - shell: /bin/bash
    - creates:
      - {{ home }}/.virtualenvs/dentally-docs
    - require:
      - stooj_dotfiles_dentally_docs_virtualenv_script_downloaded

{% if osfamily == 'Debian' %}
stooj_dotfiles_dentally_heroku_repo_managed:
  pkgrepo.managed:
    - humanname: heroku
    - name: deb https://cli-assets.heroku.com/apt ./
    - file: /etc/apt/sources.list.d/heroku.list
    - key_url: https://cli-assets.heroku.com/apt/release.key
    - refresh_db: true

stooj_dotfiles_dentally_heroku_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.heroku|yaml }}
    {% if osfamily == 'Debian' %}
    - require:
      - stooj_dotfiles_dentally_heroku_repo_managed
    {% endif %}
{% endif %}

stooj_dotfiles_dentally_hubot_cloned:
  git.latest:
    - name: git@github.com:dentally/hubot.git
    - target: {{ home }}/programming/dentally/hubot
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_dentally_directory_managed

stooj_dotfiles_dentally_houston_cloned:
  git.latest:
    - name: git@github.com:dentally/houston.git
    - target: {{ home }}/programming/dentally/houston
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_dentally_directory_managed

stooj_dotfiles_dentally_heroku_pg_dump_repo_cloned:
  git.latest:
    - name: git@gitlab.com:dentally/heroku-pg-dump.git
    - target: {{ home }}/programming/dentally/heroku-pg-dump
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

stooj_dotfiles_dentally_specs_document_cloned:
  git.latest:
    - name: git@gitlab.com:dentally/dentally-spec-documents.git
    - target: {{ home }}/programming/dentally/dentally-spec-documents
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

{# Disabling dentally vpn
 # stooj_dotfiles_dentally_openvpn_manager_installed:
 #   pkg.installed:
 #     - pkgs: {{ stooj_dotfiles.pkgs.openvpn|yaml }}
 # 
 # stooj_dotfiles_dentally_perimeter_ca_cert_downloaded:
 #   file.managed:
 #     - name: /etc/openvpn/safervpn.com.ca.crt
 #     - source: https://www.safervpn.com/files/openvpnconfigs/safervpn.com.ca.crt
 #     - source_hash: 37e7a82c14e997dfdefe62f1eacceb72c8962eb4d158c644da300ca57698e6ebcb2bae3668c631c6522dfbf6d710ecae17b307bff0cb35a1dfbfda27457d1764
 #     - user: root
 #     - group: root
 #     - mode: 644
 #     - makedirs: True
 # 
 # stooj_dotfiles_dentally_perimeter_network_manager_config_managed:
 #   file.managed:
 #     - name: /etc/NetworkManager/system-connections/Dentally - Perimeter 81.nmconnection
 #     - source: salt://stooj_dotfiles/templates/dentally/Dentally - Perimeter 81.nmconnection.jinja
 #     - template: jinja
 #     - user: root
 #     - group: root
 #     - mode: 600
 #     - context:
 #         config: {{ stooj_dotfiles.dentally.perimeter|json }}
#}

{# Wireguard setup
 # From https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/
 # CONF_FILE="dentally.conf"
 # nmcli connection import type wireguard file "$CONF_FILE"
 #}
