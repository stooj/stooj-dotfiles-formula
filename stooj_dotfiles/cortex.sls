{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_manage_cortex_config:
  file.managed:
    - name: {{ home }}/.cortex/config
    - source: salt://stooj_dotfiles/templates/cortex/config.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - filemode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
