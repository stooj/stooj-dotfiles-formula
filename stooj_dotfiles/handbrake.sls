{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set os = salt['grains.get']('os', 'Debian') %}

stooj_dotfiles_handbrake_presets_managed:
  file.managed:
    - name: {{ home }}/.config/ghb/presets.json
    - source: salt://stooj_dotfiles/files/handbrake/presets.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{% if os == 'Ubuntu' %}
stooj_dotfiles_handbrake_snap_presets_managed:
  file.managed:
    - name: {{ home }}/snap/handbrake-jz/134/.config/ghb/presets.json
    - source: salt://stooj_dotfiles/files/handbrake/presets.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endif %}
