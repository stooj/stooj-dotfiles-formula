{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_darktable_config_directory_exists:
  file.directory:
    - name: {{ home }}/.config/darktable
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 700
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_darktable_config_rc_exists:
  file.managed:
    - name: {{ home }}/.config/darktable/darktablerc
    - user: {{ username }}
    - group: {{ username }}
    - replace: False
    - require:
      - stooj_dotfiles_darktable_config_directory_exists

stooj_dotfiles_darktable_config_rc_output_syntax_managed:
  file.replace:
    - name: {{ home }}/.config/darktable/darktablerc
    - pattern: '^plugins/imageio/storage/disk/file_directory=.*'
    - repl: plugins/imageio/storage/disk/file_directory=$(FILE_FOLDER/RAW/EDIT)/$(FILE_NAME/RAW/EDIT)
    - append_if_not_found: True
    - require:
      - stooj_dotfiles_darktable_config_directory_exists
      - stooj_dotfiles_darktable_config_rc_exists

stooj_dotfiles_darktable_config_rc_last_creator_managed:
  file.replace:
    - name: {{ home }}/.config/darktable/darktablerc
    - pattern: '^ui_last/import_last_creator=.*'
    - repl: ui_last/import_last_creator=Stoo Johnston
    - append_if_not_found: True
    - require:
      - stooj_dotfiles_darktable_config_directory_exists
      - stooj_dotfiles_darktable_config_rc_exists
