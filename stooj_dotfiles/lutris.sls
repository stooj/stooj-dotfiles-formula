{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_lutris_system_yaml_managed:
  file.managed:
    - name: {{ home }}/.config/lutris/system.yml
    - source: salt://stooj_dotfiles/files/lutris/system.yml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
