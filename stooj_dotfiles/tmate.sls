{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_tmate_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.tmate|yaml }}

stooj_dotfiles_tmate_config_managed:
  file.managed:
    - name: {{ home }}/.tmate.conf
    - source: salt://stooj_dotfiles/files/tmate/tmate.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
