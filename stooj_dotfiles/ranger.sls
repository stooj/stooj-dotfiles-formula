{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_ranger_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.ranger|yaml }}

{% for dir in [
  'ranger',
  'ranger/plugins',
  'ranger/colorschemes',
  ]
%}
stooj_dotfiles_ranger_{{ dir|replace('/', '_',) }}_directory_managed:
  file.directory:
    - name: {{ home }}/.config/{{ dir }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
{% endfor %}

stooj_dotfiles_ranger_config_managed:
  file.managed:
    - name: {{ home }}/.config/ranger/rc.conf
    - source: salt://stooj_dotfiles/files/ranger/rc.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_ranger_scope_managed:
  file.managed:
    - name: {{ home }}/.config/ranger/scope.sh
    - source: salt://stooj_dotfiles/files/ranger/scope.sh
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_ranger_devicons_plugin_installed:
  git.latest:
    - name: https://github.com/alexanderjeurissen/ranger_devicons
    - target: /tmp/ranger_devicons
    - require:
      - stooj_dotfiles_git_installed
    - unless: ls {{ home }}/.config/ranger/plugins/devicons.py && ls {{ home }}/.config/ranger/plugins/devicons_linemode.py
      {# unless prevents this state from working.
      # See https://github.com/saltstack/salt/issues/50218
      - ls {{ home }}/.config/ranger/plugins/devicons.py
      - ls {{ home }}/.config/ranger/plugins/devicons_linemode.py
      #}

stooj_dotfiles_ranger_install_devicons_plugin:
  cmd.run:
    - name: make
    - cwd: /tmp/ranger_devicons
    - runas: {{ username }}
    - require:
      - stooj_dotfiles_ranger_devicons_plugin_installed
      - stooj_dotfiles_ranger_ranger_plugins_directory_managed
    - creates:
      - {{ home }}/.config/ranger/plugins/devicons.py
      - {{ home }}/.config/ranger/plugins/devicons_linemode.py
