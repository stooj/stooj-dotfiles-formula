{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_rtv_config_managed:
  file.managed:
    - name: {{ home }}/.config/rtv/rtv.cfg
    - source: salt://stooj_dotfiles/files/rtv/rtv.cfg
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_rtv_bin_script_managed:
  file.managed:
    - name: {{ home }}/bin/rtv
    - source: salt://stooj_dotfiles/templates/rtv/rtv.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
