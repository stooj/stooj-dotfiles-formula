{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_pass_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.pass|yaml }}

stooj_dotfiles_qtpass_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.qtpass|yaml }}

stooj_dotfiles_pass_repo_cloned:
  git.latest:
    - name: {{ stooj_dotfiles.passconfig.git_repo }}
    - target: {{ home }}/.password-store
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}

stooj_dotfiles_pindy_pass_repo_cloned:
  git.latest:
    - name: {{ stooj_dotfiles.pindypassconfig.git_repo }}
    - target: {{ home }}/.pindy-password-store
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
