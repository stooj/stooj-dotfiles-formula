{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_salt_programming_directory_managed:
  file.directory:
    - name: {{ home }}/programming/salt
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

{% set missing_salt_repos = [
  'docker-formula',
  ]
%}

{% for name, data in stooj_dotfiles.salt_repos.items() %}

stooj_dotfiles_salt_{{ name|replace('-', '_') }}_repo_cloned:
  git.latest:
    - name: {{ data['origin'] }}
    - target: {{ home }}/programming/salt/{{ name }}
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_programming_directory_managed

{% for remote, url in data.items() %}
{% if remote == 'origin' %}
{# Do nothing - already dealt with origin #}
{% else %}
stooj_dotfiles_salt_{{ name|replace('-', '_') }}_repo_{{ remote }}_added:
  cmd.run:
    - name: git remote add {{ remote }} {{ url }}
    - cwd: {{ home }}/programming/salt/{{ name }}
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_programming_directory_managed
      - stooj_dotfiles_salt_{{ name|replace('-', '_') }}_repo_cloned
    - unless:
      - grep 'remote "{{ remote }}"' {{ home }}/programming/salt/{{ name }}/.git/config
{% endif %}
{% endfor %}
{% endfor %}

{#
{% set missing_salt_repos = [
  'docker-formula',
  ]
%}

{% for repo in missing_salt_repos %}
stooj_dotfiles_salt_{{ repo|replace('-', '_') }}_repo_missing:
  file.absent:
    - name: {{ home }}/programming/salt/{{ repo }}
{% endfor %}
#}
