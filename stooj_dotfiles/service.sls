{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

stooj_dotfiles_service_running:
  service.running:
    - name: {{ stooj_dotfiles.service }}
    - enable: True
