{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_ncmpcpp_config_managed:
  file.managed:
    - name: {{ home }}/.config/ncmpcpp/config
    - source: salt://{{ username }}_dotfiles/files/ncmpcpp/config
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
