{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_kodi_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.kodi|yaml }}

stooj_dotfiles_kodi_sources_managed:
  file.managed:
    - name: {{ home }}/.kodi/userdata/sources.xml
    - source: salt://stooj_dotfiles/files/kodi/sources.xml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0664
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_kodi_estuary_skin_settings_managed:
  file.managed:
    - name: {{ home }}/.kodi/userdata/addon_data/skin.estuary/settings.xml
    - source: salt://stooj_dotfiles/files/kodi/skin.estuary/settings.xml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_kodi_emby_settings_managed:
  file.managed:
    - name: {{ home }}/.kodi/userdata/addon_data/plugin.video.emby/settings.xml
    - source: salt://stooj_dotfiles/templates/kodi/plugin.video.emby/settings.xml.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - replace: False
    - context:
        config: {{ stooj_dotfiles.kodiconfig }}
    - require:
      - user: {{ username }}

stooj_dotfiles_kodi_guisettings_managed:
  file.managed:
    - name: {{ home }}/.kodi/userdata/guisettings.xml
    - source: salt://stooj_dotfiles/files/kodi/guisettings.xml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0664
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

stooj_dotfiles_kodi_advancedsettings_managed:
  file.managed:
    - name: {{ home }}/.kodi/userdata/advancedsettings.xml
    - source: salt://stooj_dotfiles/files/kodi/advancedsettings.xml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0664
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{#
stooj_dotfiles_kodi_addons_downloaded:
  file.managed:
    - name: '/tmp/addons.tar.gz'
    - source: {{ stooj_dotfiles.kodiconfig.addons_url }}
    - source_hash: {{ stooj_dotfiles.kodiconfig.addons_hash }}
    - user: root
    - group: root
    - mode: 0644
    - unless:
      - 'ls {{ home }}/.kodi/addons/plugin.video.emby'

stooj_dotfiles_kodi_addons_extracted:
  archive.extracted:
    - name: {{ home }}/.kodi
    - source: /tmp/addons.tar.gz
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - stooj_dotfiles_kodi_addons_downloaded
#}
