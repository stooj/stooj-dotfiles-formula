{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% set os_family = salt['grains.get']('os_family', 'None') %}

{% set role = salt.grains.get('role', '') %}
{% if os_family == 'Arch' %}
{% if role == 'personal-computer' or role == 'cio-laptop' %}
stooj_dotfiles_vim_minimal_removed:
  pkg.removed:
    - name: vim
{% endif %}
{% endif %}

stooj_dotfiles_vim_installed:
  pkg.installed:
    {% if role == 'personal-computer' or role == 'cio-laptop' %}
    - pkgs: {{ stooj_dotfiles.pkgs.gvim|yaml }}
    {% else %}
    - pkgs: {{ stooj_dotfiles.pkgs.vim|yaml }}
    {% endif %}
    {% if os_family == 'Arch' %}
    {% if role == 'personal-computer' or role == 'cio-laptop' %}
    - require:
      - stooj_dotfiles_vim_minimal_removed
    {% endif %}
    {% endif %}

{# Base vim config files #}
{% for file in [
  'vimrc',
  'gvimrc',
  'vimrc.bundles',
  'vimrc.indoors',
  'vimrc.outdoors',
  ]
%}
stooj_dotfiles_vim_{{ file|replace('.', '_') }}_managed:
  file.managed:
    - name: {{ home }}/.{{ file }}
    - source: salt://stooj_dotfiles/files/vim/{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{# After ftplugin files #}
{% for file in [
    'c',
    'dot',
    'markdown',
    'perl',
    'python',
    'rst',
    'rtv',
    'sh',
    'text',
    'tex',
    'yaml'
  ]
%}
stooj_dotfiles_vim_ftplugin_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.vim/after/ftplugin/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/neovim/after/ftplugin/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{# Indentation files #}
{% for file in [
    'css',
    'html',
    'javascript',
    'markdown',
    'nginx',
    'php',
    'rst',
    'yaml',
  ]
%}
stooj_dotfiles_vim_after_indent_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.vim/after/indent/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/vim/after/indent/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{# syntax files #}
stooj_dotfiles_vim_after_syntax_moin_managed:
  file.managed:
    - name: {{ home }}/.vim/after/syntax/moin.vim
    - source: salt://stooj_dotfiles/files/vim/after/syntax/moin.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

{# Doc files #}
stooj_dotfiles_vim_doc_bootstrap_managed:
  file.managed:
    - name: {{ home }}/.vim/doc/bootstrap.txt
    - source: salt://stooj_dotfiles/files/vim/doc/bootstrap.txt
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

{# ftdetect files #}
{% for file in [
    'fstab',
    'htmldjango',
    'moin',
    'nginx',
    'rst',
    'rtv',
    'sieve',
    'sls',
    'sudoers',
    'systemd',
    'todo',
  ]
%}
stooj_dotfiles_vim_ftdetect_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.vim/ftdetect/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/vim/ftdetect/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{# Additional vim indent file #}
stooj_dotfiles_vim_indent_css_managed:
  file.managed:
    - name: {{ home }}/.vim/indent/css.vim
    - source: salt://stooj_dotfiles/files/vim/indent/css.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

{% for file in [
    'perl',
    'php',
    'python',
    'snippets',
    'tex',
  ]
%}
stooj_dotfiles_vim_skeletons_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.vim/skeletons/skeleton.{{ file }}
    - source: salt://stooj_dotfiles/files/neovim/skeletons/skeleton.{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
    'c',
    'htmldjango',
    'html',
    'markdown',
    'moin',
    'php',
    'python',
    'sh',
    'todo',
    'vim',
  ]
%}
stooj_dotfiles_vim_ultisnippets_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.vim/ultisnippets/{{ file }}.snippets
    - source: salt://stooj_dotfiles/files/neovim/ultisnippets/{{ file }}.snippets
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}
