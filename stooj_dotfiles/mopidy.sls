{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_mopidy_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.mopidy|yaml }}

stooj_dotfiles_mopidy_config_managed:
  file.managed:
    - name: {{ home }}/.config/mopidy/mopidy.conf
    - source: salt://stooj_dotfiles/templates/mopidy/mopidy.conf.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - mode: 0644
    - makedirs: True
    - context:
        config: {{ stooj_dotfiles.mopidyconfig|json }}
    - require:
      - user: {{ username }}
