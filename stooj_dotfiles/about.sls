{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_about_python_sphinx_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.python_sphinx|yaml }}

stooj_dotfiles_about_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:stooj/about.git
    - target: {{ home }}/programming/about
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

stooj_dotfiles_about_make_html:
  cmd.run:
    - name: make html
    - cwd: {{ home }}/programming/about
    - runas: {{ username }}
    - onchanges:
      - stooj_dotfiles_about_repo_cloned
    - require:
      - stooj_dotfiles_about_python_sphinx_installed
      - stooj_dotfiles_about_repo_cloned
