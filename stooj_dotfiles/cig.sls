{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = stooj_dotfiles.username %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', '') %}

stooj_dotfiles_cig_binary_managed:
  file.managed:
    - name: {{ home }}/bin/cig
    - source: https://github.com/stevenjack/cig/releases/download/v0.1.5/cig_Linux_x86_64
    - source_hash: 59104a064318cef14a22e95aeccf9707262d472992fa3aa2468765b869246cf5210886b41e50b305e958f1e726dcc4a11741aac91ecc155b9b184f95870ca963
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - user: {{ username }}

stooj_dotfiles_cig_config_managed:
  file.managed:
    - name: {{ home }}/.cig.yaml
    - source: salt://stooj_dotfiles/templates/cig/cig.yaml
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - context:
        role: {{ role }}
    - require:
      - user: {{ username }}
