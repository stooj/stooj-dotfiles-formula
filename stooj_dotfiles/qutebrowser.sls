{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_qutebrowser_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.qutebrowser|yaml }}

stooj_dotfiles_qutebrowser_wrapper_script_managed:
  file.managed:
    - name: {{ home }}/bin/qutebrowser
    - source: salt://stooj_dotfiles/files/qutebrowser/bin-files/qutebrowser-wrapper-script
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - require:
      - user: {{ username }}


stooj_dotfiles_qutebrowser_quickmarks_managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/quickmarks
    - source: salt://stooj_dotfiles/templates/qutebrowser/quickmarks.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.qutebrowserconfig }}
    - require:
      - user: {{ username }}

stooj_dotfiles_qutebrowser_bookmarks_managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/bookmarks/urls
    - source: salt://stooj_dotfiles/templates/qutebrowser/urls.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.qutebrowserconfig }}
    - require:
      - user: {{ username }}

{% for file in [
  'config.py',
  ]
%}
stooj_dotfiles_qutebrowser_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/{{ file }}
    - source: salt://stooj_dotfiles/templates/qutebrowser/{{ file }}.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
  'keys.conf',
  'qutebrowser.conf',
  ]
%}
stooj_dotfiles_qutebrowser_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/qutebrowser/{{ file }}
    - source: salt://stooj_dotfiles/files/qutebrowser/{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_qutebrowser_hidpi_script_managed:
  {% if salt.grains.get('hidpi', False) %}
  file.managed:
    - name: {{ home }}/bin/qutebrowser-hidpi
    - source: salt://stooj_dotfiles/files/qutebrowser/bin-files/qutebrowser-hidpi
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
  {% else %}
  file.absent:
    - name: {{ home }}/bin/qutebrowser-hidpi
  {% endif %}

stooj_dotfiles_qutebrowser_scripts_repo_managed:
  git.latest:
    - name: git@odo.stooj.org:configs-stoo/qutebrowser-scripts.git
    - target: {{ home }}/programming/qutebrowser-scripts
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

{% for script in [
  'markdownlink',
  'plainlink',
  'rstlink',
  ]
%}
stooj_dotfiles_qutebrowser_{{ script }}_script_managed:
  file.managed:
    - name: {{ home }}/.local/share/qutebrowser/userscripts/{{ script }}
    - source: salt://stooj_dotfiles/files/qutebrowser/scripts/{{ script }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% set role = salt.grains.get('role', '') %}
{% if role == 'cio-laptop' %}
{% set profiles = ['work'] %}
{% else %}
{% set profiles = [
  'default',
  'pindy',
  'work'
  ]
%}
{% endif %}
{% for profile in profiles %}
stooj_dotfiles_qutebrowser_{{ profile }}_script_installed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-qutebrowser
    - source: salt://stooj_dotfiles/templates/qutebrowser/launch-qutebrowser-profile.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      profile: {{ profile }}
    - require:
      - user: {{ username }}

stooj_dotfiles_qutebrowser_{{ profile }}_hidpi_script_managed:
  {% if salt.grains.get('hidpi', False) %}
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-qutebrowser-hidpi
    - source: salt://stooj_dotfiles/templates/qutebrowser/launch-qutebrowser-hidpi-profile.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - context:
      profile: {{ profile }}
    - require:
      - user: {{ username }}
  {% else %}
  file.absent:
    - name: {{ home }}/bin/{{ profile }}-qutebrowser-hidpi
  {% endif %}
{% endfor %}

stooj_dotfiles_qutebrowser_qutescript_repo_cloned:
  git.latest:
    - name: https://github.com/hiway/python-qutescript.git
    - target: /tmp/python-qutescript
    - require:
      - stooj_dotfiles_git_installed
    - unless: ls {{ home }}/.local/lib/python3.7/site-packages/qutescript

stooj_dotfiles_qutebrowser_install_qutescript_plugin:
  cmd.run:
    - name: pip install --user /tmp/python-qutescript
    - cwd: /tmp/python-qutescript
    - runas: {{ username }}
    - require:
      - stooj_dotfiles_qutebrowser_qutescript_repo_cloned
    - creates:
      - {{ home }}/.local/lib/python3.7/site-packages/qutescript
{#
{% for session in [
  'default',
  'work'
  ]
%}
stooj_dotfiles_qutebrowser_{{ session }}_session_managed:
  file.managed:
    - name: {{ home }}/.local/share/qutebrowser/sessions/{{ session }}.yml
    - source: salt://stooj_dotfiles/files/qutebrowser/sessions/{{ session }}.yml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}
#}
