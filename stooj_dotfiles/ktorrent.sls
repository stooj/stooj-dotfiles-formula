{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_ktorrent_config_managed:
  file.managed:
    - name: {{ home }}/.config/ktorrentrc
    - source: salt://stooj_dotfiles/files/ktorrent/ktorrentrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    {# Don't replace the file if it already exists. This template is designed
       to provide a skeleton for ktorrent, not an entire configuration file #}
    - replace: False
    - require:
      - user: {{ username }}
