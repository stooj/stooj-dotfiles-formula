{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_pip_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.pip|yaml }}

{% for package in stooj_dotfiles.pip.pkgs %}
stooj_dotfiles_python_{{ package }}_installed:
  pip.installed:
    - name: {{ package }}
    {# - user: {{ username }} #}
    - require:
      - stooj_dotfiles_pip_installed
      - user: {{ username }}
{% endfor %}
