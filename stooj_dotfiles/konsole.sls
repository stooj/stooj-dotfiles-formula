{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_konsole_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.konsole|yaml }}

stooj_dotfiles_konsole_rc_config_managed:
  file.managed:
    - name: {{ home }}/.config/konsolerc
    - source: salt://stooj_dotfiles/files/konsole/konsolerc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_konsole_default_profile_managed:
  file.managed:
    - name: {{ home }}/.local/share/konsole/default.profile
    - source: salt://stooj_dotfiles/files/konsole/default.profile
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_konsole_gruvbox_dark_colorscheme_managed:
  file.managed:
    - name: {{ home }}/.local/share/konsole/Gruvbox_dark.colorscheme
    - source: salt://stooj_dotfiles/files/konsole/Gruvbox_dark.colorscheme
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_konsole_stoos_profile_managed:
  file.managed:
    - name: {{ home }}/.local/share/konsole/stoos-profile.profile
    - source: salt://stooj_dotfiles/files/konsole/stoos-profile.profile
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
