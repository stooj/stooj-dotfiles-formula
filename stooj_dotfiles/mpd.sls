{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_mpd_config_managed:
  file.managed:
    - name: {{ home }}/.config/mpd/mpd.conf
    - source: salt://{{ username }}_dotfiles/files/mpd/mpd.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_mpd_playlists_directory_managed:
  file.directory:
    - name: {{ home }}/.config/mpd/playlists
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: True
