{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set os = salt['grains.get']('os', None) %}
{% set os_family = salt['grains.get']('os_family', None) %}

{% if os == 'Ubuntu' %}
stooj_dotfiles_neovim_ppa_managed:
  pkgrepo.managed:
    - humanname: neovim stable
    - name: ppa:neovim-ppa/stable
    - file: /etc/apt/sources.list.d/neovim.list
    - keyid: 8231B6DD
    - keyserver: keyserver.ubuntu.com
    - refresh_db: true
{% endif %}

stooj_dotfiles_neovim_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.neovim|yaml }}
    {% if os == 'Ubuntu' %}
    - require:
      - stooj_dotfiles_neovim_ppa_managed
    {% endif %}

stooj_dotfiles_virtualenvwrapper_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.virtualenvwrapper|yaml }}

{% if os_family == 'Debian' %}
stooj_dotfiles_neovim_libevent_pkg.installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.libevent|yaml }}
{% endif %} 

stooj_dotfiles_neovim_init_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/init.vim
    - source: salt://stooj_dotfiles/files/neovim/init.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_neovim_bundle_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/bundles.vim
    - source: salt://stooj_dotfiles/files/neovim/bundles.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

{% for file in [
    'html.handlebars',
    'markdown',
    'rst',
    'ruby',
  ]
%}
stooj_dotfiles_neovim_indent_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/indent/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/neovim/after/indent/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
    'csv',
    'rst',
    'sls',
  ]
%}
stooj_dotfiles_neovim_ftdetect_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/ftdetect/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/neovim/after/ftdetect/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
    'c',
    'csv',
    'dot',
    'go',
    'javascript',
    'markdown',
    'perl',
    'python',
    'rst',
    'rtv',
    'ruby',
    'sh',
    'text',
    'tex',
    'yaml'
  ]
%}
stooj_dotfiles_neovim_ftplugin_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/after/ftplugin/{{ file }}.vim
    - source: salt://stooj_dotfiles/files/neovim/after/ftplugin/{{ file }}.vim
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
    'perl',
    'php',
    'python',
    'snippets',
    'tex',
  ]
%}
stooj_dotfiles_neovim_skeletons_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/skeletons/skeleton.{{ file }}
    - source: salt://stooj_dotfiles/files/neovim/skeletons/skeleton.{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for file in [
    'all',
    'c',
    'htmldjango',
    'html',
    'markdown',
    'moin',
    'php',
    'python',
    'sh',
    'todo',
    'vim',
  ]
%}
stooj_dotfiles_neovim_ultisnippets_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/nvim/ultisnippets/{{ file }}.snippets
    - source: salt://stooj_dotfiles/files/neovim/ultisnippets/{{ file }}.snippets
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_neovim_python_virtualenvs_script_downloaded:
  file.managed:
    - name: /tmp/create_virtualenvs
    - source: salt://stooj_dotfiles/templates/neovim/create_virtualenvs.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
    - unless:
      - ls {{ home }}/.virtualenvs/neovim2
      - ls {{ home }}/.virtualenvs/neovim3

stooj_dotfiles_neovim_python_virtualenvs_created:
  cmd.run:
    - name: bash /tmp/create_virtualenvs
    - runas: {{ username }}
    - cwd: {{ home }}
    - shell: /bin/bash
    - creates:
      - {{ home }}/.virtualenvs/neovim2
      - {{ home }}/.virtualenvs/neovim3
    - require:
      - stooj_dotfiles_neovim_python_virtualenvs_script_downloaded

{% for name, packages in stooj_dotfiles.linters.items() %}
stooj_dotfiles_neovim_{{ name }}_linter_packages_installed:
  pkg.installed:
    - pkgs: {{ packages|yaml }}
{% endfor %}

{{ username }}_dotfiles_neovim_proselint_config_managed:
  file.managed:
    - name: {{ home }}/.proselintrc
    - source: salt://stooj_dotfiles/files/neovim/proselint.config
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_alex_configuration_managed:
  file.managed:
    - name: {{ home }}/.alexrc
    - source: salt://stooj_dotfiles/files/neovim/alexrc.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
