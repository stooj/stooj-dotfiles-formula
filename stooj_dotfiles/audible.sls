{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_audible_src_directory_managed:
  file.directory:
    - name: {{ home }}/src
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

stooj_dotfiles_audible_aaxtomp3_repo_cloned:
  git.latest:
    - name: https://github.com/KrumpetPirate/AAXtoMP3.git
    - target: {{ home }}/src/AAXtoMP3
    - user: {{ username }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_audible_src_directory_managed

stooj_dotfiles_audible_aaxtomp3_symlink_created:
  file.symlink:
    - name: {{ home }}/bin/aaxtomp3
    - target: {{ home }}/src/AAXtoMP3/AAXtoMP3
    - require:
      - user: {{ username }}
      - file: {{ home }}/src
      - file: {{ home }}/bin
      - stooj_dotfiles_audible_src_directory_managed
      - stooj_dotfiles_audible_aaxtomp3_repo_cloned

stooj_dotfiles_audible_audible_activator_cloned:
  git.latest:
    - name: https://github.com/inAudible-NG/audible-activator.git
    - target: {{ home }}/src/audible-activator
    - user: {{ username }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_audible_src_directory_managed

stooj_dotfiles_audible_audible_activator_script_managed:
  file.managed:
    - name: {{ home }}/bin/audible-activator
    - source: salt://stooj_dotfiles/templates/audible/activator.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
        home: {{ home }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/bin

{# vim:set et sw=2 ts=4 ft=sls: #}
