{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% for config in [
  'movies',
  'tmm',
  'tvShows',
]
%}
stooj_dotfiles_tiny_media_manager_{{ config }}_config_managed:
  file.managed:
    - name: {{ home }}/.tiny-media-manager/data/{{ config }}.json
    - source: salt://stooj_dotfiles/files/tiny_media_manager/{{ config }}.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}
