{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', '') %}

stooj_dotfiles_remind_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.remind|yaml }}

stooj_dotfiles_remind_config_managed:
  file.managed:
    - name: {{ home }}/.reminders
    - source: salt://stooj_dotfiles/files/remind/reminders
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{% if role == 'cio-laptop' %}
{% else %}
stooj_dotfiles_remind_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:stooj/calendar.git
    - target: {{ home }}/programming/calendar
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
{% endif %}
