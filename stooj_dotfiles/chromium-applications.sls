{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_ensure_chromium_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.chromium|yaml }}

stooj_dotfiles_ensure_dex_installed:
  pkg.installed:
    - name: dex

{% set wanted = stooj_dotfiles.chromiumapplications.wanted %}
{% for file in wanted %}
stooj_dotfiles_chromium_application_{{ file }}_desktop_file:
  file.managed:
    - name: {{ home }}/.local/share/applications/{{ file }}.desktop
    - source: salt://stooj_dotfiles/templates/chromium-applications/{{ file }}.desktop.jinja
    - template: jinja
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      chromium_location: {{ stooj_dotfiles.config.chromium_location }}

stooj_dotfiles_chromium_application_{{ file }}_bin_file:
  file.managed:
    - name: {{ home }}/bin/{{ file }}
    - source: salt://stooj_dotfiles/templates/chromium-applications/launch-script.jinja
    - template: jinja
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      filename: {{ file }}

{% endfor %}

{% set missing = stooj_dotfiles.chromiumapplications.missing %}
{% for file in missing %}
stooj_dotfiles_chromium_application_{{ file }}_desktop_file_removed:
  file.absent:
    - name: {{ home }}/.local/share/applications/{{ file }}.desktop

stooj_dotfiles_chromium_application_{{ file }}_bin_file_removed:
  file.absent:
    - name: {{ home }}/bin/{{ file }}
{% endfor %}
