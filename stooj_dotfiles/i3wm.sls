{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set platform = salt['grains.get']('chassis', 'desktop') %}
{% set hidpi = salt['grains.get']('hidpi', False) %}
{% set role = salt.grains.get('role', '') %}

stooj_dotfiles_random_wallpaper_programmes_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.feh|yaml }}

stooj_dotfiles_random_wallpaper_script_removed:
  file.absent:
    - name: {{ home }}/bin/random-i3-wallpaper

stooj_dotfiles_change_wallpaper_old_script_removed:
  file.absent:
    - name: {{ home }}/bin/change-wallpaper

stooj_dotfiles_change_wallpaper_script_managed:
  file.managed:
    - name: {{ home }}/bin/change_wallpaper
    - source: salt://stooj_dotfiles/files/i3wm/scripts/change_wallpaper.py
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}

{% for systemd_file in [
  'change_i3_background.service',
  'change_i3_background.timer',
  ]
%}
stooj_dotfiles_change_wallpaper_{{ systemd_file }}_managed:
  file.managed:
    - name: {{ home }}/.config/systemd/user/{{ systemd_file }}
    - source: salt://stooj_dotfiles/files/i3wm/systemd/{{ systemd_file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_compton_config_managed:
  file.managed:
    - name: {{ home }}/.config/compton.conf
    - source: salt://stooj_dotfiles/files/i3wm/configs/compton.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_dunst_config_managed:
  file.managed:
    - name: {{ home }}/.config/dunst/dunstrc
    - source: salt://stooj_dotfiles/files/i3wm/configs/dunstrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_i3wm_config_managed:
  file.managed:
    - name: {{ home }}/.i3/config
    - source: salt://stooj_dotfiles/templates/i3wm/configs/i3config.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - context:
        hidpi: {{ hidpi }}
    - require:
      - user: {{ username }}

stooj_dotfiles_i3wm_blocks_managed:
  file.managed:
    - name: {{ home }}/.i3/i3blocks.conf
    - source: salt://stooj_dotfiles/templates/i3wm/configs/i3blocks.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - context:
        platform: {{ platform }}
    - require:
      - user: {{ username }}

stooj_dotfiles_i3_autostart_script_managed:
  file.managed:
    - name: {{ home }}/.i3/autostart
    - source: salt://stooj_dotfiles/templates/i3wm/scripts/autostart.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}

{% if salt['grains.get']('has_battery', False) %}
stooj_dotfiles_i3wm_blocks_battery_script_managed:
  file.managed:
    - name: {{ home }}/.i3/scripts/battery
    - source: salt://stooj_dotfiles/files/i3wm/scripts/battery
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
{% endif %}

{% for script in [
  'disk',
  'exit_menu',
  'load_average',
  'memory',
  'volume',
  'weather',
  ]
%}
stooj_dotfiles_i3wm_blocks_{{ script }}_script_managed:
  file.managed:
    - name: {{ home }}/.i3/scripts/{{ script }}
    - source: salt://stooj_dotfiles/files/i3wm/scripts/{{ script }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_background_pictures_created:
  file.symlink:
    - name: /home/stooj/.desktop-backgrounds
    - target: /home/stooj/pictures/wallpapers/eve-online
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - user: {{ username }}
    - onlyif: ls /home/stooj/pictures/wallpapers/eve-online
