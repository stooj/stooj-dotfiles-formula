{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', '') %}

stooj_dotfiles_firefox_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.firefox|yaml }}

{% if role == 'cio-laptop' %}
{% set profiles = ['work'] %}
{% else %}
{% set profiles = [
  'default',
  'pindy',
  'netflix',
  'stock',
  'work',
  ]
%}
{% endif %}
{% for profile in profiles %}
stooj_dotfiles_firefox_{{ profile }}_script_installed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-iceweasel
    - source: salt://stooj_dotfiles/templates/firefox/launch-iceweasel-profile.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      profile: {{ profile }}
    - require:
      - user: {{ username }}
{% endfor %}

{% for profile in [
  'school']
%}
stooj_dotfiles_firefox_{{ profile }}_script_removed:
  file.absent:
    - name: {{ home }}/bin/{{ profile }}-iceweasel
{% endfor %}
