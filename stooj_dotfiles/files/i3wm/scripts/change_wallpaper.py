#!/usr/bin/env python3

import datetime
import os
import subprocess
import sys


STRETCH_ACROSS_DUAL_MONITOR = False
XINERAMA_THRESHOLD = 200000

def to_str(bytes_or_str):
    if isinstance(bytes_or_str, bytes):
        value = bytes_or_str.decode('utf-8')
    else:
        value = bytes_or_str
    return value


def to_bytes(bytes_or_str):
    if isinstance(bytes_or_str, str):
        value = bytes_or_str.encode('utf-8')
    else:
        value = bytes_or_str
    return value

def select_image():
    image_dir = os.path.join(os.path.expanduser('~'), '.desktop-backgrounds')
    files = os.listdir(image_dir)
    files.sort()

    now = datetime.datetime.utcnow()
    this_minute_as_integer = int(now.strftime("%Y%m%d%H%M"))

    selected_file_index = this_minute_as_integer % len(files)
    selected_image = files[selected_file_index]
    return os.path.join(image_dir, selected_image)


def detect_ratio(image):
    detect_ratio_process = [
        "identify",
        "-format",
        "%[fx:round(100000*abs((1/1)-(w/h)))]:%M\n",
        image,
    ]
    if STRETCH_ACROSS_DUAL_MONITOR:
        ratio_process = subprocess.run(
            detect_ratio_process,
            capture_output=True,
        )
        result = int(to_str(ratio_process.stdout).split(":")[0])
    else:
        result = 0

    return result


def set_background(image, ratio):
    feh_cmd = ["feh", "--bg-max", image]
    if ratio > XINERAMA_THRESHOLD and STRETCH_ACROSS_DUAL_MONITOR:
        feh_cmd.insert(2, "--no-xinerama")

    print("Running {} to set background".format(" ".join(feh_cmd)))
    return subprocess.run(feh_cmd).returncode


def main():
    background_image = select_image()
    print("Selected {} as background image".format(background_image))
    ratio = detect_ratio(background_image)
    print("Detected {} as ratio".format(ratio))
    return set_background(background_image, ratio)


if __name__ == '__main__':
    sys.exit(main())
