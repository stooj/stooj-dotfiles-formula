#!/usr/bin/env python2
# vim:fileencoding=utf-8 tabstop=4 shiftwidth=4 softtabstop=4

from __future__ import print_function
from subprocess import check_output
import sys


def get_password(email, password_location):
    return check_output(
        "pass {}/{}".format(
            password_location, email), shell=True).splitlines()[0]


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print(
            "Need to provide username and password location",
            file=sys.stderr
        )
        sys.exit(1)
    user = sys.argv[1]
    location = sys.argv[2]
    print(get_password(user, location))
