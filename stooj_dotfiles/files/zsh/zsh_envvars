#!/bin/zsh

#######################################################################
#                        Environment Variables                        #
#######################################################################

# Configure editor
if [ $(command -v nvim) ]; then
    PREFERRED_EDITOR=$(which nvim)
else
    PREFERRED_EDITOR=$(which vim)
fi

# Set git preferred editor
export GIT_EDITOR=$PREFERRED_EDITOR

# Set SVN preferred editor
export SVN_EDITOR=$PREFERRED_EDITOR

# In fact, lets just go for everything neovim
export EDITOR=$PREFERRED_EDITOR
export VISUAL=$PREFERRED_EDITOR

# Except manpages. Use vimpager for them
if command -v vimpager > /dev/null; then
    export PAGER='vimpager'
else
    export PAGER='less'
fi

# Add personal python packages to default pythonpath
export PYTHONPATH=$HOME/programming/python-packages
# Disable pyc files. I don't use em.
export PYTHONDONTWRITEBYTECODE=1

# Configure go path
GOPATH=$HOME/programming/go
export GOPATH

# Add my GPG key
export GPG_KEY=1161756B
export PERSONAL_GPG_KEY=8ECCDCE1
export STORAGE_GPG_KEY=A899EFC5FB201982
export SCOTLAND_GPG_KEY=33CCC6165C9D8D00
export SCOTLAND_NEWBIEKEY=FD02F8B709D64EA8
export SCOTLAND_NANOKEY=5E1127E696D38F92

# Set my virtualenvs directory
export WORKON_HOME=$HOME/.virtualenvs

# Temporarily set yubikey to be ONLY GPG key for password store
# PASSWORD_STORE_KEY=A899EFC5FB201982
# export PASSWORD_STORE_KEY

# Set papersize
PAPERSIZE=a4
export PAPERSIZE

# Configure fzf to use ripgrep
if command -v rg > /dev/null; then
    FZF_DEFAULT_COMMAND='rg --files'
    export FZF_DEFAULT_COMMAND
fi

# Default browser for terminal-applications
export BROWSER=/usr/bin/qutebrowser

# Select default ledger file 
export LEDGER_FILE=$HOME/programming/accounts/main.ledger

# Set tmuxinator config location
export TMUXINATOR_CONFIG=$HOME/.config/tmuxinator

export TIMEWARRIORDB="$HOME/programming/timewarrior"
export TASKRC="$HOME/.config/task/taskrc"
