[Appearance]
AntiAliasFonts=true
ColorScheme=Gruvbox_dark
Font=UbuntuMono Nerd Font,13,-1,5,50,0,0,0,0,0

[General]
Environment=TERM=xterm-256color,COLORTERM=truecolor
Name=stoos-profile
Parent=FALLBACK/

[Terminal Features]
UrlHintsModifiers=67108864
