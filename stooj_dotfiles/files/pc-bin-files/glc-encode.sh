#!/bin/bash

# See http://youtu.be/bxSXMSW24Dw

#glc-play $1 -a 1 -o glc.wav
#glc-play $1 -o - -y 1 | ffmpeg -i - -i glc.wav -preset ultrafast -acodec flac -vcodec libx264 output.mkv
#glc-play $1 -o - -y 1 | ffmpeg -i - -preset ultrafast -vcodec copy output.mkv
glc-play $1 -o - -y 1 | ffmpeg -i - -preset ultrafast -vcodec libx264 ${1%.*}.mkv
#rm glc.wav
