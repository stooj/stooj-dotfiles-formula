" ============
" User bundles
" ============

" Appearance
" ----------

"" Display the indentation levels with thin vertical lines
call dein#add('Yggdroot/indentLine')
"" Adds file type glyphs/icons to popular vim plugins: NERDTree, vim-airline,
"" Powerline, Unite, vim-startify and more
call dein#add('ryanoasis/vim-devicons')
"" Lean & mean status/tabline for vim that's light as air
call dein#add('vim-airline/vim-airline')

" Colours
" -------

"" one colorscheme pack to rule them all
call dein#add('flazz/vim-colorschemes')
"" Retro groove color scheme for vim
call dein#add('morhetz/gruvbox')
"" Gruvbox colorscheme is actually included in vim-colorschemes, but it is
"" slightly out of date

" Autocomplete
" ------------

"" Dark powered asyncronous completion framework for neovim/Vim8
call dein#add('Shougo/deoplete.nvim')
"" :substitute preview
call dein#add('osyo-manga/vim-over')

" Git
" ---

"" A Git wrapper so awesome, it should be illegal
call dein#add('tpope/vim-fugitive')
"" Shows a git diff in the gutter (sign column) and stages/undoes hunks
call dein#add('airblade/vim-gitgutter')
"" An extension to fugitive.vim for gitlab support
call dein#add('shumphrey/fugitive-gitlab.vim')

" Syntax
" ------

"" Asyncronous linting/fixing for Vim and Language Server Protocol (LSP)
"" integration
call dein#add('w0rp/ale')
"" EditorConfig helps developers define and maintain consistent coding styles
"" between different editors and IDEs.
call dein#add('editorconfig/editorconfig-vim')

" Building
" --------

"" Asyncronous build and test dispatcher
call dein#add('tpope/vim-dispatch')
"" Neovim support for vim-dispatch
call dein#add('radenling/vim-dispatch-neovim.git')
"" Run your tests at the speed of thought
call dein#add('janko-m/vim-test.git')

" File management
" ---------------

" Fuzzy find
" ^^^^^^^^^^

"" A command-line fuzzy finder
call dein#add('junegunn/fzf', { 'merged': '0' })
"" Default configuration and bindings for fzf
call dein#add('junegunn/fzf.vim', { 'depends': 'fzf' })

" Semantic find
" ^^^^^^^^^^^^^

"" Granular project configuration
call dein#add('tpope/vim-projectionist.git')

" Content find
" ^^^^^^^^^^^^

"" Helps you win at grep
call dein#add('mhinz/vim-grepper.git')

" File browser
" ^^^^^^^^^^^^

"" A tree explorer plugin for vim
call dein#add('scrooloose/nerdtree')
"" A plugin of NERDTree showing git status
call dein#add('Xuyuanp/nerdtree-git-plugin')

" Snippets
" --------

"" The ultimate snippet solution for Vim
call dein#add('SirVer/ultisnips')
"" vim-snipmate default snippets (Also works with ultisnips)
call dein#add('honza/vim-snippets')
"" Vim skeletons - pre-populate empty files with boilerplate
call dein#add('tobyS/skeletons.vim')

" Tags
" ----

"" Base plugin for other xolox plugins
call dein#add('xolox/vim-misc')
"" Automated tag file generation and syntax highlighting of tags
call dein#add('xolox/vim-easytags')
"" Displays tags in a window, ordered by scope
call dein#add('majutsushi/tagbar')

" Languages
" ---------

"" A solid language pack for vim - language packs for many, many languages
call dein#add('sheerun/vim-polyglot')

" Go
" ^^

call dein#add('fatih/vim-go', {'on_ft': 'go'})

" Jinja
" ^^^^^

"" An up-to-date jinja2 syntax file
call dein#add('Glench/Vim-Jinja2-Syntax', {'on_ft': 'jinja'})

" i3
" ^^

call dein#add('PotatoesMaster/i3-vim-syntax', {'on_ft': 'i3'})

" Ledger
" ^^^^^^

"" Ledger filetype
call dein#add('ledger/vim-ledger', {'on_ft': 'ledger'})
"" Plugin for hledger files - omni completion, unite source, mappings
call dein#add('anekos/hledger-vim', {'on_ft': 'hledger'})

" Python
" ^^^^^^

"" deoplete source for python
call dein#add('zchee/deoplete-jedi', {'on_ft': 'python'})
"" vim files for editing salt files
call dein#add('saltstack/salt-vim', {'on_ft': 'sls'})

" RST
" ^^^
"" Riv is a vim plugin for taking notes with reStructuredText.
call dein#add('gu-fan/riv.vim')

" Ruby
" ^^^^

"" Ruby on rails power tools
call dein#add('tpope/vim-rails', {'on_ft': 'ruby'})
"" It's like vim-rails, without the rails
call dein#add('tpope/vim-rake', {'on_ft': 'ruby'})
"" Run Rspec specs from vim
" call dein#add('thoughtbot/vim-rspec')
"" Refactoring tool for ruby in vim
" call dein#add('ecomba/vim-ruby-refactoring')


" Tools
" -----

"" Generate a fast shell prompt with powerline symbols and airline colors
call dein#add('edkolev/promptline.vim')
"" Simple tmux statusline generator with support for powerline symbols and
"" statusline/airline/lightline integration
call dein#add('edkolev/tmuxline.vim')

" Previewers
" ----------
"" Preview rst documents instantly
call dein#add('Rykka/InstantRst')

" Other Niceties
" --------------

"" Quoting/parenthesizing made simple
call dein#add('tpope/vim-surround')
"" enable repeating supported plugin maps with '.'
call dein#add('tpope/vim-repeat')
"" Use CTRL-A/CTRL-X to increment dates, times and more
call dein#add('tpope/vim-speeddating')
"" Comment stuff out
call dein#add('tpope/vim-commentary')
"" Pairs of handy bracket mappings
call dein#add('tpope/vim-unimpaired')
"" Visualize your vim undo tree
call dein#add('sjl/gundo.vim.git')
"" Vim Table mode for instant table creation
call dein#add('dhruvasagar/vim-table-mode', {'on_ft': ['markdown', 'rst']})
"" Language agnostic vim plugin for folding and motion based on indentation
call dein#add('pseewald/vim-anyfold')
"" Move between vim panes and tmux panes seamlessly
call dein#add('christoomey/vim-tmux-navigator')
