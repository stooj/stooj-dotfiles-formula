setlocal textwidth=80 colorcolumn=80
setlocal spell spelllang=en_gb
" Table-mode
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='
" Default to make html
set makeprg=make\ html
