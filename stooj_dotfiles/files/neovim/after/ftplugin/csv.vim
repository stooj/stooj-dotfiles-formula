"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                 This file is managed by Saltstack.                  "
"                      Any changes will be lost.                      "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

nmap ,1 AExam<Esc>j
nmap ,2 AScale & Polish<Esc>j
nmap ,3 AExam + Scale & Polish<Esc>j
nmap ,4 AContinuing Treatment<Esc>j
nmap ,5 AEmergency<Esc>j
nmap ,6 AReview<Esc>j
nmap ,7 AOther<Esc>j
nmap ,8 ANew Patient Exam<Esc>j
nmap ,9 ABroken Tooth<Esc>j
nmap ,x0 AToothache<Esc>j
nmap ,x1 APain<Esc>j
nmap ,x2 ALost Filling<Esc>j
nmap ,x3 ALost Crown<Esc>j
