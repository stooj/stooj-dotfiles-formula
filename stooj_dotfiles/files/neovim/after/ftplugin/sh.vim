" Make sure lines don't go past 88 characters
setlocal colorcolumn=88

" Enable indent-based folding
setlocal foldmethod=indent
setlocal foldlevelstart=0
