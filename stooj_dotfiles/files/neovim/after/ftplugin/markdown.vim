setlocal textwidth=80 colorcolumn=80
setlocal spell spelllang=en_gb
setlocal conceallevel=0
" Table mode
let g:table_mode_corner='|'

let b:ale_fixers = ['prettier']
