" Wrap at 72 chars for comments.
setlocal formatoptions=cq textwidth=72 foldignore= wildignore+=*.py[co] colorcolumn=80

" Enable indent-based folding
setlocal foldmethod=indent
setlocal foldlevelstart=0
