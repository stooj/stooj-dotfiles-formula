" =========
" Dein core
" =========

if &compatible
  set nocompatible
endif

let dein_readme=expand('~/.local/share/dein/repos/github.com/Shougo/dein.vim/README.md')
if !filereadable(dein_readme)
  echo "Installing dein"
  echo ""
  silent !curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > /tmp/installer.sh
  silent !sh /tmp/installer.sh ~/.local/share/dein
endif

set runtimepath+=~/.local/share/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.local/share/dein')
  call dein#begin('~/.local/share/dein')

  " Let dein manage dein
  " Required:
  call dein#add('~/.local/share/dein/repos/github.com/Shougo/dein.vim')

  "" Include user's extra bundle
  if filereadable(expand("~") . "/.config/nvim/bundles.vim")
    source ~/.config/nvim/bundles.vim
  endif

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
if dein#check_install()
  call dein#install()
endif

" ==============
" System options
" ==============
" Hitting E363: pattern uses more memory than 'maxpattern' for rst files (try
" README.rst in stooj_dotfiles repo. (probably due to some dodgy regex in the
" rst plugin, or the vim regex engine.)
" Increase the maxmempattern limit (default is 1000)
set maxmempattern=5000

" ==================
" Neovim Virtualenvs
" ==================

let g:python_host_prog = $WORKON_HOME . '/neovim2/bin/python'
let g:python3_host_prog = $WORKON_HOME . '/neovim3/bin/python'

" ============
" Key Bindings
" ============

" General
" -------

" Leader key
let mapleader = ','
" jk in insert mode will escape
inoremap jk <Esc>

" Clean search (highlight)
nnoremap <silent> <leader><space> :noh<cr>

" List shortcut for showing invisible characters
nmap <leader>l :set list!<CR>

" Typo defences
" -------------

cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q

" NVIM terminal
" -------------

if has('nvim')
    tnoremap <Esc> <C-\><C-n>
    tnoremap <C-v><Esc> <Esc>
endif

" Git
" ---

noremap <Leader>ga :Gwrite<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gsh :!git push<CR>
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gb :Gblame<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>gr :Gremove<CR>
noremap <Leader>go :Gbrowse<CR>
let g:fugitive_gitlab_domains = ['https://odo.stooj.org']

" File Browsing
" -------------

map <C-t> :NERDTreeToggle<CR>

" Fuzzy Finding
" -------------

nnoremap <Leader>f :<C-u>FZF<CR>
nnoremap <Leader>/ :<C-u>BLines<CR>
nnoremap <Leader>b :<C-u>Buffers<CR>
nnoremap <Leader>: :<C-u>History:<CR>
nnoremap <Leader>s :<C-U>Snippets<CR>
nnoremap <Leader>t :<C-U>Tags<CR>

" Grepper
" -------
nnoremap <Leader>* :Grepper -cword -noprompt<CR>
nnoremap <Leader>a :Grepper -tool rg<CR>
" rg, ack, grep

" Linting error navigation
" ------------------------
nmap <silent> [W <Plug>(ale_first)
nmap <silent> [w <Plug>(ale_previous)
nmap <silent> ]w <Plug>(ale_next)
nmap <silent> ]W <Plug>(ale_last)

" Testing
" -------

nnoremap <Leader>ts :TestSuite<CR>
nnoremap <Leader>tf :TestFile<CR>
nnoremap <Leader>tn :TestNearest<CR>
nnoremap <Leader>tl :TestLast<CR>

" Tagbar
" ------

nmap <F8> :TagbarToggle<CR>

" Undo
" ----
nnoremap <leader>u :GundoToggle<CR>

" ===============
" Visual Settings
" ===============

syntax on
set ruler
set number
set relativenumber

"" Automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =

"" Zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>z :wincmd _<CR>:wincmd \|<CR>
nnoremap <leader>= :wincmd =<CR>

"" Use modeline overrides
set modeline
set modelines=10

" Make listchar symbols a bit more explicit
set listchars=tab:▸\ ,eol:¬

" ALE
" ---

let g:ale_sign_error = ' '
let g:ale_sign_warning = ''
let g:ale_fixers = {
            \ '*': ['remove_trailing_lines', 'trim_whitespace'],
            \}

" Gitgutter
" ---------

let g:gitgutter_sign_added = ''
let g:gitgutter_sign_modified = ''
let g:gitgutter_sign_removed = ''
let g:gitgutter_sign_removed_first_line = ''
let g:gitgutter_sign_modified_removed = ''

" Gruvbox
" -------
let g:gruvbox_italic=0
let g:gruvbox_contrast_dark='soft'
set background=dark
colorscheme gruvbox

" ====================
" Indentation Settings
" ====================
" Set the default indentation settings in case a filetype doesn't set them
" specifically.
" These can be overridden by ft indentation rules in $VIMCONFIG/after/indent

set tabstop=4
set softtabstop=0
set shiftwidth=4
set expandtab

" ==============
" Search options
" ==============
set hlsearch
set incsearch
set ignorecase
set smartcase
" Make replacements default to global ones (everything on the line). If you
" want to just change the first instance, add a g to the end of the search
set gdefault

" =============
" Autocmd Rules
" =============

" Remember cursor position
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" ===============
" Plugin settings
" ===============

" Airline
" -------
let g:airline_theme='gruvbox'
let g:airline_powerline_fonts = 1

if $TMATE == '1'
    let g:airline_powerline_fonts = 0
else
    let g:airline_powerline_fonts = 1
endif

" Anyfold
" -------
autocmd FileType * AnyFoldActivate
set foldlevel=99

" Deoplete
" --------
let g:deoplete#enable_at_startup = 1

" Easytags
" --------
"  Easytags is not able to detect ctags on Arch any more.
"  See: 
"  - https://github.com/xolox/vim-easytags/pull/133

let g:easytags_suppress_ctags_warning = 1

" FZF
" ---

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})

" Grepper
" -------

let g:grepper = {}
let g:grepper.tools = ['grep', 'git', 'rg']
nmap gs <plug>(GrepperOperator)
xmap gs <plug>(GrepperOperator)

" indentLine
" ----------

let g:indentLine_setConceal = 0

" Nerdtree
" --------

" Close neovim if the only window open is NERDtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Promptline
" ----------
" sections (a, b, c, x, y, z, warn) are optional
let g:promptline_preset = {
        \'a' : [ promptline#slices#host(), promptline#slices#python_virtualenv() ],
        \'b' : [ promptline#slices#user() ],
        \'c' : [ promptline#slices#cwd() ],
        \'y' : [ promptline#slices#vcs_branch(), promptline#slices#git_status() ],
        \'warn' : [ promptline#slices#last_exit_code() ]}

" Riv
" ---

"" Riv keybindings conflict with ultisnips. See 
"" https://github.com/SirVer/ultisnips/issues/512#issuecomment-198223678
"" for more details.
let g:riv_ignored_imaps = "<Tab>,<S-Tab>"

" Tagbar
" ------

let g:tagbar_autoclose = 1

" Ultisnips
" ---------

let g:UltiSnipsSnippetDirectories = ["UltiSnips", "ultisnippets"]
let g:UltiSnipsSnippetsDir = "~/.config/nvim/ultisnippets"

" Vim Test
" --------

let test#strategy = "dispatch"
