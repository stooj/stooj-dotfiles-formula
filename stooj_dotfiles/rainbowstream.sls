{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_rainbowstream_config_managed:
  file.managed:
    - name: {{ home }}/.rainbow_config.json
    - source: salt://stooj_dotfiles/files/rainbowstream/rainbow_config.json
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_rainbowstream_bin_script_managed:
  file.managed:
    - name: {{ home }}/bin/rainbowstream
    - source: salt://stooj_dotfiles/templates/rainbowstream/rainbowstream.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
