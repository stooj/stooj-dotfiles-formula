{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

include:
  - .service

stooj_dotfiles:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs|yaml }}
    - require_in:
        - service: stooj_dotfiles_service_running
