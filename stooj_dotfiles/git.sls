{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}
{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_git_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.git|yaml }}

{% set excludefile = stooj_dotfiles.gitconfig["core.excludesFile"]|replace('~', home) %}
stooj_dotfiles_git_ignore_file_managed:
  file.managed:
     - name: {{ excludefile }}
     - source: salt://stooj_dotfiles/files/git/gitignore
     - user: {{ username }}
     - group: {{ username }}
     - mode: 0644
     - require:
       - user: {{ username }}

{% for key, value in stooj_dotfiles.gitconfig.items() %}
stooj_dotfiles_gitconfig_{{ key }}:
  git.config_set:
    - name: {{ key }}
    - value: {{ value }}
    - user: {{ stooj_dotfiles.username }}
    - global: True
    - require:
      - user: {{ stooj_dotfiles.username }}
{% endfor %}

{# vim:set et sw=2 ts=4 ft=sls: #}
