{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_ruby_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.ruby|yaml }}

{# 
  This state is a work-around for salt's lack of user gems support.
  For more information, see 
  https://github.com/saltstack/salt/issues/31821#issuecomment-278006045
 #}
stooj_dotfiles_ruby_gemrc_file_managed:
  file.managed:
    - name: {{ home }}/.gemrc
    - source: salt://stooj_dotfiles/files/ruby/gemrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 643
    - require:
      - user: {{ username }}

{% for package in stooj_dotfiles.gem.pkgs %}
stooj_dotfiles_gem_{{ package }}_installed:
  gem.installed:
    - name: {{ package }}
    - user: {{ username }}
    {# The following two options need to be set to avoid
     # https://github.com/saltstack/salt/issues/51069
     #}
    - rdoc: true
    - ri: true
    - require:
      - stooj_dotfiles_ruby_installed
      - stooj_dotfiles_ruby_gemrc_file_managed
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_gemrc_file_removed:
  file.absent:
    - name: {{ home }}/.gemrc
    - require:
      - stooj_dotfiles_ruby_installed
      - stooj_dotfiles_ruby_gemrc_file_managed
      - user: {{ username }}
      {% for package in stooj_dotfiles.gem.pkgs %}
      - stooj_dotfiles_gem_{{ package }}_installed
      {% endfor %}

stooj_dotfiles_ruby_rbenv_repo_cloned:
  git.latest:
    - name: https://github.com/rbenv/rbenv.git
    - target: {{ home }}/.rbenv
    - user: {{ username }}
    - require:
      - user: {{ username }}

stooj_dotfiles_ruby_rbenv_configure_script_run:
  cmd.run:
    - name: {{ home }}/.rbenv/src/configure
    - cwd: {{ home }}/.rbenv/
    - runas: {{ username }}
    - onchanges:
      - stooj_dotfiles_ruby_rbenv_repo_cloned
    - require: 
      - user: {{ username }}
      - stooj_dotfiles_ruby_rbenv_repo_cloned

stooj_dotfiles_ruby_rbenv_make_script_run:
  cmd.run:
    - name: make -C src
    - cwd: {{ home }}/.rbenv/
    - runas: {{ username }}
    - onchanges:
      - stooj_dotfiles_ruby_rbenv_repo_cloned
    - require: 
      - user: {{ username }}
      - stooj_dotfiles_ruby_rbenv_repo_cloned
      - stooj_dotfiles_ruby_rbenv_configure_script_run

stooj_dotfiles_ruby_ruby_build_directory_managed:
  file.directory:
    - name: {{ home }}/.rbenv/plugins
    - user: {{ username }}
    - group: {{ username }}
    - require: 
      - user: {{ username }}
      - stooj_dotfiles_ruby_rbenv_repo_cloned
      - stooj_dotfiles_ruby_rbenv_configure_script_run
      - stooj_dotfiles_ruby_rbenv_make_script_run

stooj_dotfiles_ruby_ruby_build_installed:
  git.latest:
    - name: https://github.com/rbenv/ruby-build.git
    - target: {{ home }}/.rbenv/plugins/ruby-build
    - user: {{ username }}
    - require:
      - stooj_dotfiles_ruby_ruby_build_directory_managed
