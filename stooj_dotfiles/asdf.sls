{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}
{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_asdf_repo_cloned:
  git.latest:
    - name: https://github.com/asdf-vm/asdf.git
    - rev: v0.7.8
    - target: {{ home }}/.asdf
    - user: {{ username }}
    - require:
      - user: {{ username }}
