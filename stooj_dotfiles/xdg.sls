{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_xdg_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.xdg|yaml }}

stooj_dotfiles_xdg_config_managed:
  file.managed:
    - name: {{ home }}/.config/user-dirs.dirs
    - source: salt://stooj_dotfiles/files/xdg/user-dirs.dirs
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{% if salt['grains.get']('role', 'None') == 'personal-computer' %}
stooj_dotfiles_xdg_mimeapps_list_config_managed:
  file.managed:
    - name: {{ home }}/.config/mimeapps.list
    - source: salt://stooj_dotfiles/files/xdg/mimeapps.list
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_xdg_mimeapps_list_local_managed:
  file.managed:
    - name: {{ home }}/.local/share/applications/mimeapps.list
    - source: salt://stooj_dotfiles/files/xdg/mimeapps.list
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endif %}
