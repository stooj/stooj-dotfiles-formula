stooj_dotfiles_clone_nerdfonts:
  git.latest:
    - name: https://github.com/ryanoasis/nerd-fonts.git
    - target: /srv/nerdfonts
    - depth: 1
    - rev: 2.0.0
    - require:
      - stooj_dotfiles_git_installed
    - unless: ls /usr/local/share/fonts/NerdFonts

stooj_dotfiles_install_nerdfonts:
  cmd.run:
    - name: /srv/nerdfonts/install.sh --install-to-system-path
    - require:
      - stooj_dotfiles_clone_nerdfonts
    - creates: /usr/local/share/fonts/NerdFonts

stooj_dotfiles_remove_nerdfonts:
  file.absent:
    - name: /srv/nerdfonts
    - require:
      - stooj_dotfiles_install_nerdfonts
