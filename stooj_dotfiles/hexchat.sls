{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_hexchat_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.hexchat|yaml }}


{% for file in [
  'addon_checksum',
  'chanopt',
  'ignore',
  'notify',
  'sound',
  ]
%}
stooj_dotfiles_hexchat_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/hexchat/{{ file }}.conf
    - source: salt://stooj_dotfiles/files/hexchat/{{ file }}.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

{% endfor %}

stooj_dotfiles_hexchat_hexchatconf_managed:
  file.managed:
    - name: {{ home }}/.config/hexchat/hexchat.conf
    - source: salt://stooj_dotfiles/templates/hexchat/hexchat.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.hexchatconfig.config|yaml }}
    - require:
      - user: {{ username }}

stooj_dotfiles_hexchat_servlist_managed:
  file.managed:
    - name: {{ home }}/.config/hexchat/servlist.conf
    - source: salt://stooj_dotfiles/templates/hexchat/servlist.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.hexchatconfig.serverlist|yaml }}
    - require:
      - user: {{ username }}

