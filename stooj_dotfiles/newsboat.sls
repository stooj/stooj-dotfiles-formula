{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_newsboat_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.newsboat|yaml }}

stooj_dotfiles_newsbeuter_removed:
  pkg.removed:
    - pkgs: {{ stooj_dotfiles.absentpkgs.newsbeuter|yaml }}

{% for file in [
  'config',
  'urls',
  ]
%}
stooj_dotfiles_newsboat_{{ file }}_managed:
  file.managed:
    - name: {{ home }}/.config/{{ stooj_dotfiles.directories.newsboat }}/{{ file }}
    - source: salt://stooj_dotfiles/templates/newsboat/{{ file }}.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.newsboatconfig }}
    - require:
      - user: {{ username }}
{% endfor %}
