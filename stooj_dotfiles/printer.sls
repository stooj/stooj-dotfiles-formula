{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set os_family = salt.grains.get('os_family', None) %}

{{ username }}_dotfiles_printer_lpoptions_managed:
  file.managed:
    - name: {{ home }}/.cups/lpoptions
    - source: salt://{{ username }}_dotfiles/files/printer/lpoptions
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
