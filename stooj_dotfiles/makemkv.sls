{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = stooj_dotfiles.username %}
{% set home = "/home/%s" % username %}
{% set os = salt.grains.get('os', None) %}

{% if os == 'Ubuntu' %}
{% set config_location = home ~ '/snap/makemkv/14/.MakeMKV/settings.conf' %}
{% else %}
{% set config_location = home ~ '/.MakeMKV/settings.conf' %}
{% endif %}
stooj_dotfiles_makemkv_config_managed:
  file.managed:
    - name: {{ config_location }}
    - source: salt://stooj_dotfiles/templates/makemkv/settings.conf.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - mode: 0644
    - makedirs: True
    - context:
        config: {{ stooj_dotfiles.makemkvconfig|yaml }}
    - require:
      - user: {{ username }}

stooj_dotfiles_makemkv_directory_exists:
  file.directory:
    - name: {{ stooj_dotfiles.makemkvconfig.destination_dir }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - user: {{ username }}

