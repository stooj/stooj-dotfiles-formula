{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_zsh_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.zsh|yaml }}

{% for file in [
  'zprofile',
  'zsh_envvars',
  'zshenv',
  ] %}
stooj_dotfiles_zsh_manage_{{ file }}:
  file.managed:
    - name: {{ home }}/.{{ file }}
    - source: salt://stooj_dotfiles/files/zsh/{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}
{% endfor %}


{% for file in [
  'zsh_aliases',
  'zshrc',
  ] %}
stooj_dotfiles_zsh_manage_{{ file }}:
  file.managed:
    - name: {{ home }}/.{{ file }}
    - source: salt://stooj_dotfiles/templates/zsh/{{ file }}.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_zsh_manage_airline_theme:
  file.managed:
    - name: {{ home }}/.config/oh-my-zsh-custom/themes/airline.zsh-theme
    - source: salt://stooj_dotfiles/files/zsh/airline.zsh-theme
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_zsh_antigen_cloned:
  git.latest:
    - name: https://github.com/zsh-users/antigen.git
    - target: {{ home }}/.antigen
    - user: {{ username }}

stooj_dotfiles_zsh_set_as_default_shell:
  cmd.run:
    - name: /usr/bin/chsh -s /usr/bin/zsh {{ username }}
    - require:
      - stooj_dotfiles_zsh_installed
    - unless:
      - grep stooj /etc/passwd | grep zsh
