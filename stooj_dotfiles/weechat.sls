{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', '') %}

{% for file in [
  'irc',
  'plugins',
  'sec',
  'weechat',
  ]
%}
stooj_dotfiles_weechat_{{ file }}_conf_managed:
  file.managed:
    - name: {{ home }}/.weechat/{{ file }}.conf
    - source: salt://stooj_dotfiles/templates/weechat/{{ file }}.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - context:
        config: {{ stooj_dotfiles.weechatconfig }}
        role: {{ role }}
    - require:
      - user: {{ username }}
{% endfor %}


{% for file in [
    'alias',
    'aspell',
    'buffers',
    'charset',
    'exec',
    'iset',
    'logger',
    'relay',
    'script',
    'theme',
    'trigger',
    'xfer',
  ]
%}
stooj_dotfiles_weechat_{{ file }}_conf_managed:
  file.managed:
    - name: {{ home }}/.weechat/{{ file }}.conf
    - source: salt://stooj_dotfiles/files/weechat/{{ file }}.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% if role == 'custio-laptop' %}
{% set weemoji = 'custiomoji.json' %}
{% else %}
{% set weemoji = 'weemoji.json' %}
{% endif %}
stooj_dotfiles_weechat_weemoji_managed:
  file.managed:
    - name: {{ home }}/.weechat/weemoji.json
    - source: salt://stooj_dotfiles/files/weechat/{{ weemoji }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_weechat_doebi_theme_managed:
  file.managed:
    - name: {{ home }}/.weechat/themes/doebi.theme
    - source: salt://stooj_dotfiles/files/weechat/themes/doebi.theme
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - require:
      - user: {{ username }}

{# lua plugins #}
{% for plugin in [
    'matrix',
  ]
%}
stooj_dotfiles_weechat_lua_{{ plugin }}_plugin_symlink_removed:
  file.absent:
    - name: {{ home }}/.weechat/lua/autoload/{{ plugin }}.lua

stooj_dotfiles_weechat_lua_{{ plugin }}_plugin_absent:
  file.absent:
    - name: {{ home }}/.weechat/lua/{{ plugin }}.lua
{% endfor %}

{# perl plugins #}
{% for plugin in [
    'highmon',
    'iset',
    'multiline',
    'perlexec',
  ]
%}
stooj_dotfiles_weechat_perl_{{ plugin }}_plugin_managed:
  file.managed:
    - name: {{ home }}/.weechat/perl/{{ plugin }}.pl
    - source: salt://stooj_dotfiles/files/weechat/perl/{{ plugin }}.pl
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_weechat_perl_{{ plugin }}_plugin_enabled:
  file.symlink:
    - name: {{ home }}/.weechat/perl/autoload/{{ plugin }}.pl
    - target: {{ home }}/.weechat/perl/{{ plugin }}.pl
    - makedirs: True
{% endfor %}
{% for plugin in [
    'cmdind',
  ]
%}
stooj_dotfiles_weechat_perl_{{ plugin }}_plugin_symlink_removed:
  file.absent:
    - name: {{ home }}/.weechat/perl/autoload/{{ plugin }}.pl

stooj_dotfiles_weechat_perl_{{ plugin }}_plugin_removed:
  file.absent:
    - name: {{ home }}/.weechat/perl/{{ plugin }}.pl
{% endfor %}

{# python plugins #}
{% for plugin in [
    'apply_corrections',
    'autosort',
    'buffer_autoclose',
    'colorize_nicks',
    'go',
    'notify_send',
    'queryman',
    'urlserver',
    'vimode',
    'wee_slack',
    'zerotab',
  ]
%}
stooj_dotfiles_weechat_python_{{ plugin }}_plugin_managed:
  file.managed:
    - name: {{ home }}/.weechat/python/{{ plugin }}.py
    - source: salt://stooj_dotfiles/files/weechat/python/{{ plugin }}.py
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_weechat_python_{{ plugin }}_plugin_enabled:
  file.symlink:
    - name: {{ home }}/.weechat/python/autoload/{{ plugin }}.py
    - target: {{ home }}/.weechat/python/{{ plugin }}.py
    - makedirs: True
{% endfor %}
{% for plugin in [
    'theme',
  ]
%}
stooj_dotfiles_weechat_python_{{ plugin }}_plugin_symlink_removed:
  file.absent:
    - name: {{ home }}/.weechat/python/autoload/{{ plugin }}.py

stooj_dotfiles_weechat_python_{{ plugin }}_plugin_removed:
  file.absent:
    - name: {{ home }}/.weechat/python/{{ plugin }}.py
{% endfor %}

stooj_dotfiles_weechat_python_matrix_library_cloned:
  git.latest:
    - name: https://github.com/poljar/weechat-matrix.git
    - target: /tmp/weechat-matrix
    - user: {{ username }}
    - unless:
      - ls {{ home }}/.weechat/python/venv

stooj_dotfiles_weechat_python_matrix_virtualenv_script_downloaded:
  file.managed:
    - name: /tmp/create_weechat_matrix_virtualenv
    - source: salt://stooj_dotfiles/files/weechat/create_weechat_matrix_virtualenv
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
      - stooj_dotfiles_weechat_python_matrix_library_cloned
    - unless:
      - ls {{ home }}/.weechat/python/venv

stooj_dotfiles_weechat_python_matrix_virtualenv_created:
  cmd.run:
    - name: bash /tmp/create_weechat_matrix_virtualenv
    - runas: {{ username }}
    - cwd: {{ home }}/.weechat/python
    - shell: /bin/bash
    - creates:
      - {{ home }}/.weechat/python/venv
    - require:
      - stooj_dotfiles_weechat_python_matrix_virtualenv_script_downloaded

stooj_dotfiles_weechat_python_matrix_plugin_enabled:
  file.symlink:
    - name: {{ home }}/.weechat/python/autoload/matrix.py
    - target: {{ home }}/.weechat/python/matrix.py
    - makedirs: True
