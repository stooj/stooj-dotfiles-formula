{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% for file in [
  'bash_login',
  'inputrc',
  'profile',
  'shell_prompt',
  ] %}
stooj_dotfiles_manage_{{ file }}:
  file.managed:
    - name: {{ home }}/.{{ file }}
    - source: salt://stooj_dotfiles/files/bash/{{ file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}
{% endfor %}


{% for file in [
  'bash_aliases',
  'bash_functions',
  'bash_profile',
  'bashrc',
  ] %}
stooj_dotfiles_manage_{{ file }}:
  file.managed:
    - name: {{ home }}/.{{ file }}
    - source: salt://stooj_dotfiles/templates/bash/{{ file }}.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}
{% endfor %}
