{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% if salt['grains.get']('os_family', None) == 'Debian' %}
stooj_dotfiles_todotxt_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.todotxt|yaml }}
{% endif %}

stooj_dotfiles_todotxt_config_managed:
  file.managed:
    - name: {{ home }}/.todo/config
    - source: salt://stooj_dotfiles/files/todotxt/config
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_todotxt_actions_managed:
  file.recurse:
    - name: {{ home }}/.todo.actions.d
    - source: salt://stooj_dotfiles/files/todotxt/actions
    - user: {{ username }}
    - group: {{ username }}
    - file_mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}

{% set role = salt.grains.get('role', '') %}
{% if role == 'cio-laptop' %}
{% else %}
stooj_dotfiles_todotxt_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:stooj/todo.git
    - target: {{ home }}/programming/todo
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
{% endif %}
