{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{{ username }}_dotfiles_nextcloud_config_managed:
  file.managed:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_nextcloud_sync_exclude_list_managed:
  file.managed:
    - name: {{ home }}/.config/Nextcloud/sync-exclude.lst
    - source: salt://{{ username }}_dotfiles/files/nextcloud/sync-exclude.lst
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_nextcloud_config_settings_managed:
  ini.options_present:
    - name: {{ home }}/.config/Nextcloud/nextcloud.cfg
    - sections:
        General:
          confirmExternalStorage: true
          newBigFolderSizeLimit: 1000
          optionalDesktopNotifications: true
          optionalServerNotifications: true
          useNewBigFolderSizeLimit: true
        Accounts:
          "0\\Folders\\1\\ignoreHiddenFiles": "false"
          "0\\Folders\\1\\localPath": "{{ home }}/nextcloud/itw/"
          "0\\url": "{{ stooj_dotfiles.nextcloudconfig.zero.serverurl }}"
          "1\\Folders\\2\\ignoreHiddenFiles": "false"
          "1\\Folders\\2\\localPath": "{{ home }}/nextcloud/goodteith/"
          "1\\url": "{{ stooj_dotfiles.nextcloudconfig.one.serverurl }}"
        BWLimit:
          useDownloadLimit: -1
          useUploadLimit: -1
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_nextcloud_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0755
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_nextcloud_itw_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud/itw
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - {{ username }}_dotfiles_nextcloud_directory_exists

{{ username }}_dotfiles_nextcloud_goodteith_directory_exists:
  file.directory:
    - name: {{ home }}/nextcloud/goodteith
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - {{ username }}_dotfiles_nextcloud_directory_exists

{{ username }}_dotfiles_nextcloud_autostart_file_exists:
  file.managed:
    - name: {{ home }}/.config/autostart/nextcloud.desktop
    - source: salt://{{ username }}_dotfiles/files/nextcloud/nextcloud.desktop
    - makedirs: True
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - user: {{ username }}
