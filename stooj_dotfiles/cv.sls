{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_cv_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:stooj/cv.git
    - target: {{ home }}/programming/cv
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
