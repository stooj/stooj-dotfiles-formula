{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_tmux_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.tmux|yaml }}

stooj_dotfiles_tmux_config_managed:
  file.managed:
    - name: {{ home }}/.tmux.conf
    - source: salt://stooj_dotfiles/files/tmux/tmux.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_tmuxline_config_managed:
  file.managed:
    - name: {{ home }}/.tmuxline.conf
    - source: salt://stooj_dotfiles/files/tmux/tmuxline.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
