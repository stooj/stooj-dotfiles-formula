{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_manage_clerk:
  file.managed:
    - name: {{ home }}/.config/clerk/config
    - source: salt://stooj_dotfiles/files/clerk/config
    - user: {{ username }}
    - group: {{ username }}
    - filemode: 0644
    - dirmode: 755
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    - require:
      - user: {{ username }}
