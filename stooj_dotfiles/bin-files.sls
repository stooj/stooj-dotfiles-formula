{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', None) %}
{% set chassis = salt.grains.get('chassis', None) %}

stooj_dotfiles_bin_files:
  file.recurse:
    - name: {{ home }}/bin
    - source: salt://stooj_dotfiles/files/bin-files
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0744

{% if chassis in ['laptop', 'desktop'] %}
stooj_dotfiles_workstation_bin_files:
  file.recurse:
    - name: {{ home }}/bin
    - source: salt://stooj_dotfiles/files/workstation-bin-files
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0744

stooj_dotfiles_workstation_dolphin_bin_file_removed:
  file.absent:
    - name: {{ home }}/bin/dolphin
{% endif %}

{% if role == 'personal-computer' %}
stooj_dotfiles_pc_bin_files:
  file.recurse:
    - name: {{ home }}/bin
    - source: salt://stooj_dotfiles/files/pc-bin-files
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0744
{% endif %}
