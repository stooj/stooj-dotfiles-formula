{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_geany_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.geany|yaml }}

stooj_dotfiles_geany_config_managed:
  file.managed:
    - name: {{ home }}/.config/geany/geany.conf
    - source: salt://stooj_dotfiles/templates/conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.geanyconfig.geanyconfig|json }}
    - require:
      - user: {{ username }}

stooj_dotfiles_geany_keybindings_managed:
  file.managed:
    - name: {{ home }}/.config/geany/keybindings.conf
    - source: salt://stooj_dotfiles/templates/conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
      config: {{ stooj_dotfiles.geanyconfig.keybindings }}
    - require:
      - user: {{ username }}

stooj_dotfiles_geany_ui_toolbar_managed:
  file.managed:
    - name: {{ home }}/.config/geany/ui_toolbar.xml
    - source: salt://stooj_dotfiles/files/geany/ui_toolbar.xml
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}
