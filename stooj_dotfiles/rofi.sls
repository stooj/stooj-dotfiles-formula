{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_rofi_config_managed:
  file.managed:
    - name: {{ home }}/.Xresources.d/rofi
    - source: salt://stooj_dotfiles/files/rofi/xresources-rofi-conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}


stooj_dotfiles_rofi_pass_config_managed:
  file.managed:
    - name: {{ home }}/.config/rofi-pass/config
    - source: salt://stooj_dotfiles/files/rofi/rofi-pass-config
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
