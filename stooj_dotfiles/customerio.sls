include:
  - .asdf

{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set osfamily = salt['grains.get']('os_family', None) %}
{% set os = salt['grains.get']('os', None) %}
{% set home = "/home/%s" % username %}

{% set golang_version = '1.12.14' %}


stooj_dotfiles_customerio_openvpn_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.openvpn|yaml }}

stooj_dotfiles_customerio_networkmanager_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.networkmanager|yaml }}

stooj_dotfiles_customerio_networkmanager_openvpn_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.networkmanager_openvpn|yaml }}

stooj_dotfiles_customerio_ansible_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.ansible|yaml }}

stooj_dotfiles_customerio_deploy_python_requirements_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.python_cio|yaml }}

stooj_dotfiles_customerio_directory_managed:
  file.directory:
    - name: {{ home }}/programming/customerio
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - require:
      - file: {{ home }}/programming

stooj_dotfiles_invoices_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:customerio/cio-invoices.git
    - target: {{ home }}/programming/invoices
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

stooj_dotfiles_customerio_docker_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.docker|yaml }}

stooj_dotfiles_customerio_pip2_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.pip2|yaml }}

stooj_dotfiles_customerio_docker_python_installed:
  pip.installed:
    - name: docker-py
    - reload_modules: True
    - require:
      - stooj_dotfiles_customerio_pip2_installed

stooj_dotfiles_customerio_docker_user_in_group:
  group.present:
    - name: docker
    - system: True
    - members:
      - {{ username }}

stooj_dotfiles_customerio_docker_service_managed:
  service.running:
    - name: docker
    - enable: True
    - require:
      - stooj_dotfiles_customerio_docker_installed
      - stooj_dotfiles_customerio_docker_python_installed

stooj_dotfiles_customerio_docker_mysql_present:
  docker_image.present:
    - name: mysql
    - tag: 5.7
    - require:
      - stooj_dotfiles_customerio_docker_installed
      - stooj_dotfiles_customerio_docker_python_installed
      - stooj_dotfiles_customerio_docker_service_managed

stooj_dotfiles_customerio_docker_mysql_running:
  docker_container.running:
    - name: custio-mysql
    - image: mysql:5.7
    - environment:
      - DOCKER_ALLOW_EMPTY_PASSWORD: yes
    - publish:
      - "3306:3306"
    - require:
      - stooj_dotfiles_customerio_docker_mysql_present

stooj_dotfiles_customerio_redis_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.redis|yaml }}

stooj_dotfiles_customerio_services_cloned:
  git.latest:
    - name: git@github.com:customerio/services.git
    - target: {{ home }}/programming/customerio/services
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_customerio_directory_managed

stooj_dotfiles_customerio_services_env_vars:
  file.managed:
    - name: {{ home }}/programming/customerio/services/.envrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - contents:
      - export SERVICES_CONFIG={{ home }}/programming/customerio/services/config/services.cfg
      - export GOPATH={{ home }}/programming/customerio/services
      - export USE_EXTERNAL_UI=1
      - export ASDF_GOLANG_VERSION={{ golang_version }}
    - require:
      - stooj_dotfiles_customerio_services_cloned
     

stooj_dotfiles_customerio_edge_cloned:
  git.latest:
    - name: git@github.com:customerio/edge.git
    - target: {{ home }}/programming/customerio/edge
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_customerio_directory_managed

stooj_dotfiles_customerio_edge_env_vars:
  file.managed:
    - name: {{ home }}/programming/customerio/edge/.envrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - contents:
      - export SERVICES_CONFIG={{ home }}/programming/customerio/edge/config/services.cfg
      - export GOPATH={{ home }}/programming/customerio/edge
      - export ASDF_GOLANG_VERSION={{ golang_version }}
    - require:
      - stooj_dotfiles_customerio_edge_cloned
     
stooj_dotfiles_customerio_asdf_golang_plugin_added:
  cmd.run:
    - name: {{ home }}/.asdf/bin/asdf plugin-add golang
    - cwd: {{ home }}
    - runas: {{ username }}
    - creates: {{ home }}/.asdf/plugins/golang/README.md
    - require:
      - stooj_dotfiles_asdf_repo_cloned

stooj_dotfiles_customerio_correct_go_version_installed:
  cmd.run:
    - name: {{ home }}/.asdf/bin/asdf install golang 1.10.8
    - cwd: {{ home }}
    - runas: {{ username }}
    - creates: {{ home }}/.asdf/installs/golang/1.10.8/go/README.md
    - require:
      - stooj_dotfiles_customerio_asdf_golang_plugin_added

{% for repo in [
  'deploy',
  'devops',
  'discovery',
  'dns',
  'docker',
  'go-customerio',
  'incident-response',
  'mvp',
  'opsbugs',
  'render',
  'ui',
  'unity'
  ]
%}
stooj_dotfiles_customerio_{{ repo }}_cloned:
  git.latest:
    - name: git@github.com:customerio/{{ repo }}.git
    - target: {{ home }}/programming/customerio/{{ repo }}
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_customerio_directory_managed
{% endfor %}

stooj_dotfiles_customerio_salt_directory_managed:
  file.directory:
    - name: {{ home }}/programming/customerio/salt
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - require:
      - stooj_dotfiles_customerio_directory_managed

{% for name, data in stooj_dotfiles.customerio_salt_repos.items() %}
stooj_dotfiles_customerio_{{ name|replace('-', '_') }}_cloned:
  git.latest:
    - name: {{ data['origin'] }}
    - target: {{ home }}/programming/customerio/salt/{{ name }}
    - user: {{ username }}
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_customerio_salt_directory_managed

{% for remote, url in data.items() %}
{% if remote == 'origin' %}
{# Do nothing - already dealt with origin #}
{% else %}
stooj_dotfiles_customerio_salt_{{ name|replace('-', '_') }}_repo_{{ remote }}_added:
  cmd.run:
    - name: git remote add {{ remote }} {{ url }}
    - cwd: {{ home }}/programming/customerio/salt/{{ name }}
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_customerio_salt_directory_managed
      - stooj_dotfiles_customerio_{{ name|replace('-', '_') }}_cloned
    - unless:
      - grep 'remote "{{ remote }}"' {{ home }}/programming/customerio/salt/{{ name }}/.git/config
{% endif %}
{% endfor %}
{% endfor %}

stooj_dotfiles_customerio_virtualenvs_created:
  file.managed:
    - name: /tmp/create_customerio_virtualenvs
    - source: salt://stooj_dotfiles/templates/customerio/create_virtualenvs.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
      - stooj_dotfiles_customerio_deploy_python_requirements_installed
    - unless:
      - ls {{ home }}/.virtualenvs/cio-deploy

stooj_dotfiles_customerio_netrc_file_managed:
  file.managed:
    - name: {{ home }}/.netrc
    - source: salt://stooj_dotfiles/templates/customerio/netrc.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        username: {{ username }}
        password: {{ stooj_dotfiles.customerio.github_access_token }}
    - require:
      - user: {{ username }}

stooj_dotfiles_customerio_vpn_config_managed:
  file.managed:
    - name: {{ home }}/bin/configure-cio-vpn
    - source: salt://stooj_dotfiles/templates/cio-bin-files/configure-cio-vpn.jinja
    - context: 
        google_dns: {{ stooj_dotfiles.customerio.google_dns }}
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - user: {{ username }}

stooj_dotfiles_customerio_update_repos_bin_script_managed:
  file.managed:
    - name: {{ home }}/bin/update-cio-repos
    - source: salt://stooj_dotfiles/files/cio-bin-files/update-cio-repos
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - user: {{ username }}

stooj_dotfiles_customerio_aws_config_directory_exists:
  file.directory:
    - name: {{ home }}/.aws
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 700

stooj_dotfiles_customerio_aws_config_file_managed:
  file.managed:
    - name: {{ home }}/.aws/config
    - source: salt://stooj_dotfiles/templates/aws/config.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.customerio.aws|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_customerio_aws_config_directory_exists

stooj_dotfiles_customerio_aws_credentials_file_managed:
  file.managed:
    - name: {{ home }}/.aws/credentials
    - source: salt://stooj_dotfiles/templates/aws/credentials.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 600
    - context:
        config: {{ stooj_dotfiles.customerio.aws|json }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_customerio_aws_config_directory_exists

stooj_dotfiles_customerio_tmate_staging_config_managed:
  file.managed:
    - name: {{ home }}/.tmate-staging.conf
    - source: salt://stooj_dotfiles/files/tmate/tmate-staging.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}

stooj_dotfiles_customerio_tmate_production_config_managed:
  file.managed:
    - name: {{ home }}/.tmate-production.conf
    - source: salt://stooj_dotfiles/files/tmate/tmate-production.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require:
      - user: {{ username }}

stooj_dotfiles_customerio_tmate_wrapper_bin_script_managed:
  file.managed:
    - name: {{ home }}/bin/cio-tmate
    - source: salt://stooj_dotfiles/files/cio-bin-files/cio-tmate
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - require:
      - user: {{ username }}
