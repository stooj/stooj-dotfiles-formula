{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% set accounts = stooj_dotfiles.mailconfig.get('accounts', None) %}

{% if accounts %}
stooj_dotfiles_mail_msmtprc_managed:
  file.managed:
    - name: {{ home }}/.msmtprc
    - source: salt://stooj_dotfiles/templates/mail/msmtprc.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_offlineimap_old_location_removed:
  file.absent:
    - name: {{ home }}/.mail_config/offlineimaprc

stooj_dotfiles_mail_offlineimaprc_managed:
  file.managed:
    - name: {{ home }}/.offlineimaprc
    - source: salt://stooj_dotfiles/templates/mail/offlineimaprc.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_lbdbrc_managed:
  file.managed:
    - name: {{ home }}/.mail_config/lbdb.rc
    - source: salt://stooj_dotfiles/files/mail/lbdb.rc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_pullmail_script_managed:
  file.managed:
    - name: {{ home }}/bin/pull-mail
    - source: salt://stooj_dotfiles/files/mail/pull-mail
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_systemctl_start_script_exists:
  file.managed:
    - name: {{ home }}/bin/mail-services
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{% for systemd_file in [
  'mairix.service',
  'mairix.timer',
  'pullmail.service',
  'pullmail.timer',
  'refreshaddress.service',
  'refreshaddress.timer'
  ]
%}
stooj_dotfiles_mail_{{ systemd_file }}_managed:
  file.managed:
    - name: {{ home }}/.config/systemd/user/{{ systemd_file }}
    - source: salt://stooj_dotfiles/files/mail/systemd/{{ systemd_file }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

{#
# This is disabled because service.running doesn't operate on a per-user basis
stooj_dotfiles_mail_{{ systemd_file }}_service_managed:
  service.running:
    - name: {{ systemd_file.split('.')|first }}@{{ username }}.{{ systemd_file.split('.')|last }}
    - enable: True
#}

{#
# This is disabled because calling enable as a user doesn't work via salt
stooj_dotfiles_mail_{{ systemd_file }}_service_started_by_command:
  cmd.run:
    - name: systemctl enable --user {{ systemd_file }}
    - runas: {{ username }}
    - cwd: {{ home }}
#}
stooj_dotfiles_mail_systemctl_start_script_{{ systemd_file }}_managed:
  file.append:
    - name: {{ home }}/bin/mail-services
    - text: systemctl --user start {{ systemd_file }}
    - makedirs: True
    - require:
      - stooj_dotfiles_mail_systemctl_start_script_exists
{% endfor %}

{% for config in [
    'colors',
    'mailcap',
    'pgp',
  ]
%}
stooj_dotfiles_mail_mutt_{{ config }}_managed:
  file.managed:
    - name: {{ home }}/.mail_config/mutt/{{ config }}
    - source: salt://stooj_dotfiles/files/mail/mutt/{{ config }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

{% for config in [
    'common',
    'muttrc',
    'macros',
  ]
%}
stooj_dotfiles_mail_mutt_{{ config }}_managed:
  file.managed:
    - name: {{ home }}/.mail_config/mutt/{{ config }}
    - source: salt://stooj_dotfiles/templates/mail/mutt/{{ config }}.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      mutt: {{ stooj_dotfiles.mailconfig.mutt }}
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}
{% endfor %}

{% for trigger, data in stooj_dotfiles.mailconfig.mutt.triggers.items() %}
stooj_dotfiles_mail_mutt_trigger_{{ trigger }}_managed:
  file.managed:
    - name: {{ home }}/.mail_config/mutt/{{ trigger }}
    - source: salt://stooj_dotfiles/templates/mail/mutt/trigger.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - mode: 644
    - makedirs: True
    - context:
      account: {{ data.account }}
      address: {{ data.address }}
    - require:
      - user: {{ username }}
{% endfor %}

{% for account, data in stooj_dotfiles.mailconfig.accounts.items() %}
stooj_dotfiles_mail_mutt_account_{{ account }}_managed:
  file.managed:
    - name: {{ home }}/.mail_config/mutt/{{ account }}
    - source: salt://stooj_dotfiles/templates/mail/mutt/accounts.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - mode: 644
    - makedirs: True
    - context:
      account: {{ account }}
      config: {{ data }}
    - require:
      - user: {{ username }}

{% if 'filters' in data %}
stooj_dotfiles_mail_imapfilter_{{ account }}_managed:
  file.managed:
    - name: {{ home }}/.mail_config/imapfilter/{{ account }}.lua
    - source: salt://stooj_dotfiles/templates/mail/imapfilter.lua.jinja
    - user: {{ username }}
    - group: {{ username }}
    - template: jinja
    - mode: 644
    - makedirs: True
    - context:
      account: {{ account }}
      data: {{ data }}
    - require:
      - user: {{ username }}
{% else %}
stooj_dotfiles_mail_imapfilter_{{ account }}_removed:
  file.absent:
    - name: {{ home }}/.mail_config/imapfilter/{{ account }}.lua
{% endif %}
{% endfor %}

stooj_dotfiles_mail_mutt_symlink_managed:
  file.symlink:
    - name: {{ home }}/.mutt
    - target: {{ home }}/.mail_config/mutt
    - require:
      - user: {{ username }}
      - stooj_dotfiles_mail_mutt_common_managed

stooj_dotfiles_mail_lib_get_password_script_managed:
  file.managed:
    - name: {{ home }}/.mail_config/lib/offlineimap-get-passwords.py
    - source: salt://stooj_dotfiles/files/mail/lib/offlineimap-get-passwords.py
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_lib_refreshaddress_script_managed:
  file.managed:
    - name: {{ home }}/.mail_config/lib/refreshaddress
    - source: salt://stooj_dotfiles/templates/mail/lib/refreshaddress.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - context:
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_mairix_config_managed:
  file.managed:
    - name: {{ home }}/.mail_config/mairixrc
    - source: salt://stooj_dotfiles/templates/mail/mairixrc.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 644
    - makedirs: True
    - context:
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_mairix_directory_exists:
  file.directory:
    - name: {{ home }}/.mairix
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_lib_mail_watch_script_managed:
  file.managed:
    - name: {{ home }}/.mail_config/lib/mail-watch
    - source: salt://stooj_dotfiles/templates/mail/lib/mail-watch.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - context:
      accounts: {{ stooj_dotfiles.mailconfig.accounts }}
    - require:
      - user: {{ username }}

stooj_dotfiles_mail_lib_mail_notify_script_managed:
  file.managed:
    - name: {{ home }}/.mail_config/lib/mail-notify
    - source: salt://stooj_dotfiles/templates/mail/lib/mail-notify.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
{% endif %}
