{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_byobu_files:
  file.recurse:
    - name: {{ home }}/.byobu
    - source: salt://stooj_dotfiles/files/byobu
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - file_mode: 0644
    - exclude: E@(screenrc)|(tmux.conf)|(welcome-displayed)

{% for file in [
  'screenrc',
  'tmux.conf',
  'welcome-displayed'
  ] %}
stooj_dotfiles_byobu_hidden_{{ file }}:
  file.managed:
    - name: {{ home }}/.byobu/.{{ file }}
    - source: salt://stooj_dotfiles/files/byobu/{{ file }}
    - require:
      - user: {{ username }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
{% endfor %}
