{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

/tmp/debug.txt:
  file.managed:
    - contents: |
        {{ stooj_dotfiles|yaml(False)|indent(8) }}
