{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% for profile in [
  'default',
  'pindy',
  'stock',
  'work',]
%}
stooj_dotfiles_falkon_{{ profile }}_script_installed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-falkon
    - source: salt://stooj_dotfiles/templates/falkon/launch-falkon-profile.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      profile: {{ profile }}
    - require:
      - user: {{ username }}
{% endfor %}
