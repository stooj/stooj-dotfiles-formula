{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set os_family = salt.grains.get('os_family', None) %}

{% if os_family == 'Arch' %}
stooj_dotfiles_keybase_packages_isntalled:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.keybase|yaml }}

stooj_dotfiles_keybase_system_yaml_managed:
  file.managed:
    - name: {{ home }}/.config/keybase/system.yml
    - source: salt://stooj_dotfiles/files/keybase/system.yml
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_keybase_autostart_file_exists:
  file.managed:
    - name: {{ home }}/.config/autostart/keybase_autostart.desktop
    - source: salt://{{ username }}_dotfiles/files/keybase/keybase.desktop
    - makedirs: True
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - user: {{ username }}
{% endif %}

{#
systemctl start --user keybase
systemctl enable --user keybase
keybase config set mountdir /srv/keybase
systemctl start --user kbfs
systemctl enable --user kbfs
#}
