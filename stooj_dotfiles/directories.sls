{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = stooj_dotfiles.username %}
{% set home = "/home/%s" % username %}

{% for dir in stooj_dotfiles.user_directories.base_directories %}
stooj_dotfiles_{{ dir|replace('/', '_') }}_managed:
  file.directory:
    - name: {{ home }}/{{ dir }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

# Remove uppercased versions of directories
{% if '/' not in dir %}  # Unless they are nested
stooj_dotfiles_{{ dir|title }}_base_directory_does_not_exist:
  file.absent:
    - name: {{ home }}/{{ dir|title }}
    - unless: test -L {{ home }}/{{ dir|title }}
{% endif %}
{% endfor %}

{% if salt['grains.get']('role', '') == 'personal-computer' %}
{% for dir in stooj_dotfiles.user_directories.workstation_directories %}
stooj_dotfiles_{{ dir|replace('/', '_') }}_workstation_directory_managed:
  file.directory:
    - name: {{ home }}/{{ dir }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

# Remove uppercased versions of directories
{% if '/' not in dir %}  # Unless they are nested
stooj_dotfiles_{{ dir|title }}_workstation_directory_does_not_exist:
  file.absent:
    - name: {{ home }}/{{ dir|title }}
    - unless: test -L {{ home }}/{{ dir|title }}
{% endif %}
{% endfor %}
{% endif %}

{% for link, target in stooj_dotfiles.user_directories.workstation_symlinks.items() %}

stooj_dotfiles_{{ link|replace('/', '_') }}_does_not_exist_when_symlinking:
  file.absent:
    - name: {{ home }}/{{ link }}
    - unless: test -L {{ home }}/{{ link }}

stooj_dotfiles_{{ target|replace('/', '_') }}_exists_for_symlinking:
  file.directory:
    - name: {{ target }}
    - user: {{ username }}
    - group: {{ username }}
    - dirmode: 755

stooj_dotfiles_{{ link }}_to_{{ target|replace('/', '_') }}_symlink_managed:
  file.symlink:
    - name: {{ home }}/{{ link }}
    - target: {{ target }}
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - stooj_dotfiles_{{ link|replace('/', '_') }}_does_not_exist_when_symlinking
      - stooj_dotfiles_{{ target|replace('/', '_') }}_exists_for_symlinking
{% endfor %}

{% for dir in stooj_dotfiles.user_directories.workstation_directories_after_symlinks %}
stooj_dotfiles_{{ dir|replace('/', '_') }}_after_symlinks_managed:
  file.directory:
    - name: {{ home }}/{{ dir }}
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755
    - require:
      {% for link, target in stooj_dotfiles.user_directories.workstation_symlinks.items() %}
      - stooj_dotfiles_{{ link }}_to_{{ target|replace('/', '_') }}_symlink_managed
      {% endfor %}
{% endfor %}
