{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_goodteith_directory_managed:
  file.directory:
    - name: {{ home }}/programming/goodteith
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

stooj_dotfiles_goodteith_edixml_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/edixml.git
    - target: {{ home }}/programming/goodteith/edixml
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_edixml_repo_local_remote_added:
  cmd.run:
    - name: git remote add gitlab git@gitlab.com:goodteith/goodteith-edixml.git
    - cwd: {{ home }}/programming/goodteith/edixml
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_edixml_repo_cloned
    - unless:
      - grep git@gitlab.com {{ home }}/programming/goodteith/edixml/.git/config

stooj_dotfiles_goodteith_gnudteith_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/gnudteith.git
    - target: {{ home }}/programming/goodteith/gnudteith
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_gnudteith_gitlab_remote_added:
  cmd.run:
    - name: git remote add gitlab git@gitlab.com:goodteith/gnudteith.git
    - cwd: {{ home }}/programming/goodteith/gnudteith
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_gnudteith_repo_cloned
    - unless:
      - grep git@gitlab.com {{ home }}/programming/goodteith/gnudteith/.git/config

stooj_dotfiles_goodteith_gnudteith_github_remote_added:
  cmd.run:
    - name: git remote add github git@github.com:stooj/gnudteith.git
    - cwd: {{ home }}/programming/goodteith/gnudteith
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_gnudteith_repo_cloned
    - unless:
      - grep git@github.com {{ home }}/programming/goodteith/gnudteith/.git/config

stooj_dotfiles_goodteith_goodteith_wiki_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/goodteith-wiki.wiki.git
    - target: {{ home }}/programming/goodteith/goodteith-wiki.wiki
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_ediweb_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/ediweb.git
    - target: {{ home }}/programming/goodteith/ediweb
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_goodteith_exporter_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/goodteith-exporter.git
    - target: {{ home }}/programming/goodteith/goodteith-exporter
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_goodteith_exporter_gitlab_repo_added:
  cmd.run:
    - name: git remote add gitlab git@gitlab.com:goodteith/goodteith-exporter.git
    - cwd: {{ home }}/programming/goodteith/goodteith-exporter
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_goodteith_exporter_cloned
    - unless:
      - grep 'remote "gitlab"' {{ home }}/programming/goodteith/goodteith-exporter/.git/config

{% if salt['file.directory_exists'](home ~ '/tmp/scratch-projects/dentally/migrations') %}
{% for directory in [
    'dentally_csvs',
    'goodteith_csv_exports',
    'goodteith_dbs',
    'goodteith_dsds',
  ]
%}
stooj_dotfiles_goodteith_exporter_{{ directory }}_symlink_created:
  file.symlink:
    - name: {{ home }}/programming/goodteith/goodteith-exporter/{{ directory }}
    - target: /home/stooj/tmp/scratch-projects/dentally/migrations/{{ directory }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_goodteith_goodteith_exporter_cloned
{% endfor %}
{% endif %}

stooj_dotfiles_goodteith_packer_boxes_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/packer-boxes.git
    - target: {{ home }}/programming/goodteith/packer-boxes
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_packer_boxes_gitlab_repo_added:
  cmd.run:
    - name: git remote add gitlab git@gitlab.com:goodteith/packer-boxes.git
    - cwd: {{ home }}/programming/goodteith/packer-boxes
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_packer_boxes_repo_cloned
    - unless:
      - grep 'remote "gitlab"' {{ home }}/programming/goodteith/packer-boxes/.git/config

stooj_dotfiles_goodteith_packer_windows_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/packer-windows.git
    - target: {{ home }}/programming/goodteith/packer-windows
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed

stooj_dotfiles_goodteith_packer_windows_gitlab_repo_added:
  cmd.run:
    - name: git remote add gitlab git@gitlab.com:goodteith/packer-windows.git
    - cwd: {{ home }}/programming/goodteith/packer-windows
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_packer_windows_repo_cloned
    - unless:
      - grep 'remote "gitlab"' {{ home }}/programming/goodteith/packer-windows/.git/config

stooj_dotfiles_goodteith_packer_windows_upstream_repo_added:
  cmd.run:
    - name: git remote add upstream git@github.com:joefitzgerald/packer-windows.git
    - cwd: {{ home }}/programming/goodteith/packer-windows
    - runas: {{ username }}
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
      - stooj_dotfiles_goodteith_packer_windows_repo_cloned
    - unless:
      - grep 'remote "upstream"' {{ home }}/programming/goodteith/packer-windows/.git/config

stooj_dotfiles_goodteith_goodteith_support_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:goodteith/goodteith-support.git
    - target: {{ home }}/programming/goodteith/goodteith-support
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_goodteith_directory_managed
