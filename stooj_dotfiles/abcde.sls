{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% if 'abcdeconfig' in stooj_dotfiles %}
stooj_dotfiles_abcde_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.abcde|yaml }}

stooj_dotfiles_abcde_config:
  file.managed:
    - name: {{ home }}/.abcde.conf
    - source: salt://stooj_dotfiles/templates/abcde/abcde.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - context:
        config: {{ stooj_dotfiles.abcdeconfig|yaml }}
    - require:
      - user: {{ username }}
{% endif %}

{# vim:set et sw=2 ts=4 ft=sls: #}
