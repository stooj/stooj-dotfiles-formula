{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_invent_directory_managed:
  file.directory:
    - name: {{ home }}/programming/invent
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

{% set invent_repos = {
  'nerdery-website': 'git@odo.stooj.org:invent-the-world/nerdery-website.git',
  'staff-documentation.wiki': 'git@odo.stooj.org:invent-the-world/staff-documentation.wiki.git',
  'itw-wiki.wiki': 'git@odo.stooj.org:invent-the-world/itw-wiki.wiki.git',
  'live-usb-image': 'git@odo.stooj.org:invent-the-world/live-usb-image.git',
  'digital-ocean-dns': 'git@odo.stooj.org:invent-the-world/digital-ocean-dns.git',
  'generate-lesson-sheets': 'git@odo.stooj.org:invent-the-world/generate-lesson-sheets.git',
  'itw-worksheets': 'git@odo.stooj.org:invent-the-world/itw-worksheets.git'
  }
%}

  
{% for name, address in invent_repos.items() %}
stooj_dotfiles_{{ name|replace('-', '_')|replace('.', '_') }}_cloned:
  git.latest:
    - name: {{ address }}
    - target: {{ home }}/programming/invent/{{ name }}
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_invent_directory_managed
{% endfor %}
