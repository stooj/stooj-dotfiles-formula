{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_gpg_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.gpg|yaml }}

stooj_dotfiles_gpg_directory_managed:
  file.directory:
    - name: {{ home }}/.gnupg
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 700
    - file_mode: 600
    - recurse:
      - user
      - group
      - mode
    - require:
      - stooj_dotfiles_gpg_config_managed
      - stooj_dotfiles_gpg_agent_config_managed
      - stooj_dotfiles_goodteith_saltmaster_key_managed

stooj_dotfiles_gpg_config_managed:
  file.managed:
    - name: {{ home }}/.gnupg/gpg.conf
    - source: salt://stooj_dotfiles/templates/gpg/gpg.conf.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - context:
        config: {{ stooj_dotfiles.gpgconfig|json }}
    - require:
      - user: {{ username }}

stooj_dotfiles_gpg_agent_config_managed:
  file.managed:
    - name: {{ home }}/.gnupg/gpg-agent.conf
    - source: salt://stooj_dotfiles/files/gpg/gpg-agent.conf
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - makedirs: True
    - require:
      - user: {{ username }}

stooj_dotfiles_goodteith_saltmaster_key_managed:
  gpg.present:
    - name: B3FA0F9D
    - keys: B3FA0F9D
    - keyserver: hkps://hkps.pool.sks-keyservers.net
    - user: {{ username }}
    - trust: fully
    - require:
      - user: {{ username }}

{% for key in stooj_dotfiles.gpgkeys %}
stooj_dotfiles_{{ key }}_key_managed:
  gpg.present:
    - name: {{ key }}
    - keys: {{ key }}
    - keyserver: hkps://hkps.pool.sks-keyservers.net
    - user: {{ username }}
    - trust: ultimately
    - require:
      - user: {{ username }}
{% endfor %}

{% for keyid, key in stooj_dotfiles.gpgsecretkeys.items() %}
{{ username }}_dotfiles_{{ keyid }}_secret_key_saved:
  file.managed:
    - name: {{ home }}/private-key
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0600
    - contents_pillar: {{ 'stooj_dotfiles:gpgsecretkeys:' ~ keyid }}
    - unless: 'gpg --homedir {{ home }}/.gnupg --list-secret-keys {{ keyid }}'
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_{{ keyid }}_secret_key_added:
  cmd.run:
    - name: gpg --batch --import < {{ home }}/private-key
    - cwd: {{ home }}
    - runas: {{ username }}
    - onlyif: 'ls {{ home }}/private-key'
    - require:
      - {{ username }}_dotfiles_{{ keyid }}_secret_key_saved

{{ username }}_dotfiles_{{ keyid }}_secret_key_removed:
  file.absent:
    - name: {{ home }}/private-key
    - onlyif: 'ls {{ home }}/private-key'
    - require:
      - {{ username }}_dotfiles_{{ keyid }}_secret_key_added
{% endfor %}
