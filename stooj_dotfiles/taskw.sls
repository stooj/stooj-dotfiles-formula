{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}
{% set role = salt.grains.get('role', None) %}

stooj_dotfiles_taskwarrior_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.taskwarrior|yaml }}

stooj_dotfiles_timewarrior_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.timewarrior|yaml }}

stooj_dotfiles_taskrc_config_managed:
  file.managed:
    - name: {{ home }}/.config/task/taskrc
    - source: salt://stooj_dotfiles/files/task/taskrc
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}

{% if role == 'cio-laptop' %}
{% set task_git_remote = 'customerio/cio-task' %}
{% set time_git_remote = 'customerio/cio-time' %}
{% endif %}

{% if role == 'cio-laptop' %}
stooj_dotfiles_task_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:{{ task_git_remote }}.git
    - target: {{ home }}/programming/task
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming

stooj_dotfiles_time_repo_cloned:
  git.latest:
    - name: git@odo.stooj.org:{{ time_git_remote }}.git
    - target: {{ home }}/programming/timewarrior
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
{% endif %}
