{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_tmuxinator_old_config_location_absent:
  file.absent:
    - name: {{ home }}/.tmuxinator
stooj_dotfiles_tmuxinator_config_files_managed:
  file.recurse:
    - name: {{ home }}/.config/tmuxinator
    - source: salt://stooj_dotfiles/files/tmuxinator
    - user: {{ username }}
    - group: {{ username }}
    - file_mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
