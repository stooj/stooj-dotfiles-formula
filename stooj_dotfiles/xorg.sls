{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{% for script in [
    'hidpi',
    'indoors',
    'lowdpi',
    'outdoors',
  ]
%}
stooj_dotfiles_xorg_{{ script }}_managed:
  file.managed:
    - name: {{ home }}/.Xresources.{{ script }}
    - source: salt://stooj_dotfiles/files/xorg/Xresources.{{ script }}
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endfor %}

stooj_dotfiles_xorg_toggle_screen_managed:
  file.managed:
    - name: {{ home }}/bin/toggle-screen
    - source: salt://stooj_dotfiles/files/xorg/toggle-screen
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - require:
      - user: {{ username }}
