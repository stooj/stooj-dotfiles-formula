{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_teamginstoo_src_directory_managed:
  file.directory:
    - name: {{ home }}/src
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

stooj_dotfiles_salt_teamginstoo_directory_managed:
  file.directory:
    - name: {{ home }}/programming/team-ginstoo
    - user: {{ username }}
    - group: {{ username }}
    - dir_mode: 755

stooj_dotfiles_teamginstoo_ginstoo_wiki_cloned:
  git.latest:
    - name: git@odo.stooj.org:team-ginstoo/gin-stoo.wiki.git
    - target: {{ home }}/programming/team-ginstoo/gin-stoo.wiki
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_teamginstoo_directory_managed

stooj_dotfiles_teamginstoo_recipes_cloned:
  git.latest:
    - name: git@odo.stooj.org:stooj/recipes.git
    - target: {{ home }}/programming/team-ginstoo/recipes
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_teamginstoo_directory_managed

stooj_dotfiles_teamginstoo_blog_cloned:
  git.latest:
    - name: git@odo.stooj.org:personal-site-stooj/pelican-blog.git
    - target: {{ home }}/programming/team-ginstoo/blog
    - user : stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_teamginstoo_directory_managed

stooj_dotfiles_blog_python_virtualenvs_script_downloaded:
  file.managed:
    - name: /tmp/create_blog_virtualenvs
    - source: salt://stooj_dotfiles/templates/blog/create_virtualenvs.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 744
    - makedirs: True
    - require:
      - user: {{ username }}
    - unless:
      - ls {{ home }}/.virtualenvs/blog

stooj_dotfiles_blog_python_virtualenvs_created:
  cmd.run:
    - name: bash /tmp/create_blog_virtualenvs
    - runas: {{ username }}
    - cwd: {{ home }}
    - shell: /bin/bash
    - creates:
      - {{ home }}/.virtualenvs/blog
    - require:
      - stooj_dotfiles_blog_python_virtualenvs_script_downloaded
      - stooj_dotfiles_virtualenvwrapper_installed

stooj_dotfiles_pelican_themes_cloned:
  git.latest:
    - name: https://github.com/getpelican/pelican-themes.git
    - target: {{ home }}/src/pelican-themes
    - submodules: True
    - user: stooj
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_teamginstoo_src_directory_managed

stooj_dotfiles_pelican_plugins_cloned:
  git.latest:
    - name: https://github.com/getpelican/pelican-plugins
    - target: {{ home }}/src/pelican-plugins
    - user: stooj
    - submodules: True
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_teamginstoo_src_directory_managed

stooj_dotfiles_old_pelican_themes_directory_absent:
  file.absent:
    - name: {{ home }}/programming/team-ginstoo/pelican-themes

stooj_dotfiles_pelican_plugins_directory_absent:
  file.absent:
    - name: {{ home }}/programming/team-ginstoo/pelican-plugins

stooj_dotfiles_teamginstoo_build_boxes_cloned:
  git.latest:
    - name: git@odo.stooj.org:team-ginstoo/build-boxes.git
    - target: {{ home }}/programming/team-ginstoo/build_boxes
    - user: stooj
    - identity:
      - salt://ssh/files/deploy_key
    - require:
      - user: {{ username }}
      - file: {{ home }}/programming
      - stooj_dotfiles_salt_teamginstoo_directory_managed
