{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_terminator_packages_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.terminator|yaml }}

stooj_dotfiles_terminator_config_managed:
  file.managed:
    - name: {{ home }}/.config/terminator/config
    - source: salt://stooj_dotfiles/files/terminator/config
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
