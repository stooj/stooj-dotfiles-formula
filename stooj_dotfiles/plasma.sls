{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_plasma_face_managed:
  file.managed:
    - name: {{ home }}/.face
    - source: https://files.inventtheworld.com.au/index.php/s/2MkpbL4zSSKLdBw/download
    - source_hash: ed3105e6a255a72ad37db8996b276ba09ca2b7f890574f4f3557c26e545bd870056df42623ee6ebd005c4822640c79e939a7491cd7f8d53140a96b04c8b155f3
    - show_changes: False
    - user: {{ username }}
    - group: {{ username }}
    - mode: {{ 644 }}
    - require:
      - user: {{ username }}

stooj_dotfiles_plasma_face_icon_symlink_managed:
  file.symlink:
    - name: {{ home }}/.face.icon
    - target: {{ home }}/.face
    - user: {{ username }}
    - group: {{ username }}
    - require:
      - user: {{ username }}
      - stooj_dotfiles_plasma_face_managed


stooj_dotfiles_plasma_kwinrc_managed:
  file.managed:
    - name: {{ home }}/.config/kwinrc
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: True
    - replace: False
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_plasma_kwinrc_config_settings_managed:
  ini.options_present:
    - name: {{ home }}/.config/kwinrc
    - sections:
        Compositing:
          OpenGLIsUnsafe: false
        Desktops:
          Number: 1
          Rows: 1
    - require:
      - {{ username }}_dotfiles_plasma_kwinrc_managed

stooj_dotfiles_plasma_kresurc_managed:
  file.managed:
    - name: {{ home }}/.config/kdesurc
    - source: salt://stooj_dotfiles/files/plasma/config/kdesurc
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: True
    - require:
      - user: {{ username }}

{{ username }}_dotfiles_plasma_qt5ct_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.qt5ct|yaml }}

{{ username }}_dotfiles_plasma_qt5ct_config_managed:
  ini.options_present:
    - name: {{ home }}/.config/kwinrc
    - sections:
        Appearance:
          icon_theme: oxygen
          style: Oxygen
    - require:
      - {{ username }}_dotfiles_plasma_qt5ct_installed
