{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}
{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_calibre_config_directory_exists:
  file.directory:
    - name: {{ home }}/.config/calibre
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: true
    - require: 
      - user: {{ username }}

stooj_dotfiles_calibre_conversion_directory_exists:
  file.directory:
    - name: {{ home }}/.config/calibre/conversion
    - user: {{ username }}
    - group: {{ username }}
    - makedirs: true
    - require: 
      - stooj_dotfiles_calibre_config_directory_exists

stooj_dotfiles_calibre_conversion_config_exists:
  file.managed:
    - name: {{ home }}/.config/calibre/conversion/page_setup.py
    - source: salt://stooj_dotfiles/files/calibre/conversion/page_setup.py
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - require: 
      - stooj_dotfiles_calibre_conversion_directory_exists

stooj_dotfiles_calibre_smtp_config_exists:
  file.managed:
    - name: {{ home }}/.config/calibre/smtp.py
    - source: salt://stooj_dotfiles/templates/calibre/smtp.py.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - context:
        config: {{ stooj_dotfiles.calibre|json }}
    - require: 
      - stooj_dotfiles_calibre_config_directory_exists

stooj_dotfiles_calibre_global_config_created:
  file.managed:
    - name: {{ home }}/.config/calibre/global.py
    - source: salt://stooj_dotfiles/files/calibre/global.py
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - replace: False
    - require: 
      - stooj_dotfiles_calibre_config_directory_exists

stooj_dotfiles_calibre_global_config_database_path_updated:
  file.replace:
    - name: {{ home }}/.config/calibre/global.py
    - pattern: "database_path = .*"
    - repl: "database_path = '{{ home }}/library1.db'"
    - require:
      - stooj_dotfiles_calibre_global_config_created

stooj_dotfiles_calibre_global_config_library_path_updated:
  file.replace:
    - name: {{ home }}/.config/calibre/global.py
    - pattern: 'library_path = .*'
    - repl: "library_path = '{{ stooj_dotfiles.calibre.books_location }}'"
    - require:
      - stooj_dotfiles_calibre_global_config_created
