{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

stooj_dotfiles_ssh_config_managed:
  file.managed:
    - name: {{ home }}/.ssh/config
    - source: salt://stooj_dotfiles/templates/ssh/ssh_config.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - context:
        config: {{ stooj_dotfiles.sshconfig }}
    - require:
      - user: {{ username }}

{% if salt['grains.get']('role', '') == 'personal-computer' %}
stooj_dotfiles_kde_wallet_integration_script_managed:
  file.managed:
    - name: {{ home }}/.config/autostart-scripts/ssh-add.sh
    - source: salt://stooj_dotfiles/files/ssh/ssh-add.sh
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0644
    - makedirs: True
    - require:
      - user: {{ username }}
{% endif %}
