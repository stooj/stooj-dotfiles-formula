{% from "stooj_dotfiles/map.jinja" import stooj_dotfiles with context %}

{% set username = salt['pillar.get']('stooj_dotfiles:username', 'stooj') %}
{% set home = "/home/%s" % username %}

{#
stooj_dotfiles_chrome_installed:
  pkg.installed:
    - pkgs: {{ stooj_dotfiles.pkgs.chrome|yaml }}
#}

{% for profile in [
  'default',
  'pindy',
  'netflix',
  'stock',
  'work',]
%}
stooj_dotfiles_chrome_{{ profile }}_script_installed:
  file.managed:
    - name: {{ home }}/bin/{{ profile }}-chrome
    - source: salt://stooj_dotfiles/templates/chrome/launch-chrome-profile.jinja
    - template: jinja
    - user: {{ username }}
    - group: {{ username }}
    - mode: 0744
    - makedirs: True
    - context:
      profile: {{ profile }}
    - require:
      - user: {{ username }}
{% endfor %}
